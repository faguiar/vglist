//create sorted table
var table = $(".list_table").stupidtable();

table.on("aftertablesort", function (event, data){
	var th = $(this).find("th");
	th.find(".arrow").remove();
	var dir = $.fn.stupidtable.dir;
	var arrow = data.direction === dir.ASC ? "&uarr;" : "&darr;";
	th.eq(data.column).append('<span class="arrow">' + arrow +'</span>');
});

//DELETE BUTTON
$(".list_gamerow").mouseover(function() {
	$(this).find('.deletebutton').css('display','block');	

});

$(".list_gamerow").mouseout(function() {
	$(this).find('.deletebutton').css('display','none');
});



//open form to send edited entries
function show_submit_button(input){
	$(input).parents('tr').find('.tiny_simpleButton').show();
	$(input).parents('tr').addClass("selected");
	
	$($(input).parents('tr').find('.score_input')).click(function(e) {
		e.stopPropagation();
	});
	
	$($(input).parents('tr').find('.tiny_simpleButton')).click(function(e) {
		e.stopPropagation();
	});
	
	$($(input).parents('tr').find('.status_input')).click(function(e) {
		e.stopPropagation();
	});
	
	$($(input).parents('table').find('.list_gamerow')).click(function(e){
		e.stopPropagation();
	});
	
	$($(input).parents('tr').find('.status_input_wrapper')).click(function(e){
		e.stopPropagation();
	});
}

//cancels edit list entry
$(document).click(function(e) {
	var sel_cont = $(document).find(".selected_status_input_wrapper option:selected").text();
	$('.selected_status_input_wrapper').html('<input type="text" name="value" value="'+sel_cont+'" class="input_table_cell status_input" onfocus="show_status_select(this);" />');
	$('.selected_status_input_wrapper').removeClass('selected_status_input_wrapper');
	$(document).find('.selected').find('.tiny_simpleButton').hide();
	$(document).find('.selected').removeClass("selected");
});

//show select in status_input_wrapper
function show_status_select(origin){
	var html = '';
	show_submit_button(origin);
	
	$(origin).parents('td').addClass('selected_status_input_wrapper');
	
	html+= "<select class='status_input selected_status_input'>";
	
	switch($(origin).parents('table').attr('class').split(" ")[0]){
	case 'playing':
		html+="<option value='0' selected>Playing</option><option value='1'>Wished</option><option value='2'>Completed</option><option value='3'>Not playing</option><option value='4'>On hold</option><option value='5'>Mastered</option><option value='6'>Dropped</option></select>"
		break;
		
	case 'wished':
		html+="<option value='0'>Playing</option><option value='1' selected>Wished</option><option value='2'>Completed</option><option value='3'>Not playing</option><option value='4'>On hold</option><option value='5'>Mastered</option><option value='6'>Dropped</option></select>"
		break;
	
	case 'completed':
		html+="<option value='0'>Playing</option><option value='1'>Wished</option><option value='2' selected>Completed</option><option value='3'>Not playing</option><option value='4'>On hold</option><option value='5'>Mastered</option><option value='6'>Dropped</option></select>"
		break;
	
	case 'not_playing':
		html+="<option value='0'>Playing</option><option value='1'>Wished</option><option value='2'>Completed</option><option value='3' selected>Not playing</option><option value='4'>On hold</option><option value='5'>Mastered</option><option value='6'>Dropped</option></select>"
		break;
		
	case 'onhold':
		html+="<option value='0'>Playing</option><option value='1'>Wished</option><option value='2'>Completed</option><option value='3'>Not playing</option><option value='4' selected>On hold</option><option value='5'>Mastered</option><option value='6'>Dropped</option></select>"
		break;
		
	case 'mastered':
		html+="<option value='0'>Playing</option><option value='1'>Wished</option><option value='2'>Completed</option><option value='3'>Not playing</option><option value='4'>On hold</option><option value='5' selected>Mastered</option><option value='6'>Dropped</option></select>"
		break;
		
	case 'dropped':
		html+="<option value='0'>Playing</option><option value='1'>Wished</option><option value='2'>Completed</option><option value='3'>Not playing</option><option value='4'>On hold</option><option value='5'>Mastered</option><option value='6' selected>Dropped</option></select>"				
		break;
	
	default:
		return false;
	}
	
	$(origin).parents('tr').find('.status_input_wrapper').html(html);
}

//updates values on hidden divs
function assemble_values(form){
	val_score = $(form).parents('tr').find('.score_input')[0].value;
	
	//test if score is a valid number
	if(isNaN(val_score)){
		val_score = '';
	}
	
	val_status = $(form).parents('tr').find('.status_input')[0].value;
	
	$(form).parents('tr').find('.hidden_score_input')[0].value = val_score;
	$(form).parents('tr').find('.hidden_status_input')[0].value = val_status;
	
	$(input).parents('tr').removeClass("selected");
}



