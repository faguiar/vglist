// For those who need them (< IE 9), add support for CSS functions
var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
if (!isStyleFuncSupported) {
    CSSStyleDeclaration.prototype.getPropertyValue = function(a) {
        return this.getAttribute(a);
    };
    CSSStyleDeclaration.prototype.setProperty = function(styleName, value, priority) {
        this.setAttribute(styleName,value);
        var priority = typeof priority != 'undefined' ? priority : '';
        if (priority != '') {
            // Add priority manually
            var rule = new RegExp(RegExp.escape(styleName) + '\\s*:\\s*' + RegExp.escape(value) + '(\\s*;)?', 'gmi');
            this.cssText = this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
        } 
    }
    CSSStyleDeclaration.prototype.removeProperty = function(a) {
        return this.removeAttribute(a);
    }
    CSSStyleDeclaration.prototype.getPropertyPriority = function(styleName) {
        var rule = new RegExp(RegExp.escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?', 'gmi');
        return rule.test(this.cssText) ? 'important' : '';
    }
}

// Escape regex chars with \
RegExp.escape = function(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

// The style function
jQuery.fn.style = function(styleName, value, priority) {
    // DOM node
    var node = this.get(0);
    // Ensure we have a DOM node 
    if (typeof node == 'undefined') {
        return;
    }
    // CSSStyleDeclaration
    var style = this.get(0).style;
    // Getter/Setter
    if (typeof styleName != 'undefined') {
        if (typeof value != 'undefined') {
            // Set style property
            var priority = typeof priority != 'undefined' ? priority : '';
            style.setProperty(styleName, value, priority);
        } else {
            // Get style property
            return style.getPropertyValue(styleName);
        }
    } else {
        // Get CSSStyleDeclaration
        return style;
    }
}

function generate_select(input,result,options)
{
	$("body > select.prefix_search__select").remove()
	
	var select = $("<select/>")
	select.attr("class","prefix_search__select")
	select.attr("size", Math.max(2,Math.min(6,result.length)))
	
	select.style("position", "absolute", "important")
	select.style("top", ($(input).offset().top + $(input).outerHeight()) + "px", "important")
	select.style("left", $(input).offset().left + "px", "important")
	select.style("width", $(input).outerWidth() + "px", "important")
	
	for(i=0; i<result.length; i++)
	{
		var option = $("<option/>")
		option.data("item",result[i])
		option.attr("value",result[i][options.id_field])
		option.html(result[i][options.name_field])
		
		option.appendTo(select);
	}
	
	select.children('option').click(function(e)
	{
		var item = $(this).data("item")
		var j = 0
		
		var content = options.span_content
		content = content.replace(/%name%/g,item[options.name_field])
		content = content.replace(/%id%/g,item[options.id_field])
		
		for(j in item)
		{
			content = content.replace(new RegExp("field|"+j,'g'),item[j])
		}
		
		$(input).data("prefix_search__span").html(content)
		$(input).data("prefix_search__hidden_input").val(this.value)
	})
	
	select.appendTo(document.body)
}

$.fn.prefix_search = function(url, options)
{
	"use strict"
	
	var i = 0;
	var j = 0;
	
	var def = {
		'name': undefined,
		'id_field': 'pk',
		'name_field': 'name',
		'none_id': '0',
		'none_name': 'Unknown',
		'span_content': '%name%',
		'initial_item': {
			'pk': '0',
			'name': 'Unknown',
		},
		'extra_items': [
			{
				'pk': '0',
				'name': 'Unknown',
			}
		]
	}
	
	if(options == undefined)
	{
		options = def
	}
	else
	{
		for (i in def)
		{
			if(options[i]==undefined) options[i]=def[i];
		}
	}
	
	this.attr('autocomplete','off')
	
	var span = $("<span/>")
	
	var content = options.span_content
	content = content.replace(/%name%/g,options.initial_item[options.name_field])
	content = content.replace(/%id%/g,options.initial_item[options.id_field])
	
	for(j in options.initial_item)
	{
		content = content.replace(new RegExp("field|"+j,'g'),options.initial_item[j])
	}
	
	span.html(content)
	
	span.style("margin-left","6px")
	span.insertAfter(this)
	
	this.data("prefix_search__span",span)
	
	var hidden_input = $("<input/>")
	hidden_input.attr('type','hidden')
	hidden_input.attr('value',options.initial_item[options.id_field])
	this.data("prefix_search__hidden_input",hidden_input)
	
	if(options.name != undefined)
	{
		hidden_input.attr('name',options.name)
	}
	
	hidden_input.insertAfter(this)
	
	this.data('prefix_search__cache',{'':[]})
	
	this.on("keyup focus click", function(e)
	{
		if(e.type == "click")
		{
			e.stopPropagation()
		}
		
		if(this.value in $(this).data('prefix_search__cache'))
		{
			generate_select(this,options.extra_items.concat($(this).data('prefix_search__cache')[this.value]),options)
		}
		else
		{
			if($(this).data('timeout'))
			{
				clearTimeout($(this).data('timeout'))
			}
			
			$(this).data('timeout',setTimeout(function(input)
			{
				if(!(input.value in $(input).data('prefix_search__cache')))
				{
					$.ajax({'type': "GET", 'url':url, 'dataType': 'json', 'data': {'prefix':input.value}, 'async': false, 'success':function(data){
						$(input).data('prefix_search__cache')[input.value] = data.result
					}})
				}
				
				generate_select(input,options.extra_items.concat($(input).data('prefix_search__cache')[input.value]),options)
			},1000,this))
		}
	})
	
	$(document).on("click",{'input': this},function(e)
	{
		$("body > select.prefix_search__select").remove()
		
		if(e.data.input.data('timeout'))
		{
			clearTimeout(e.data.input.data('timeout'))
		}
	})
}
