function filter_update(container){
	var sons = $('.filters_wrapper').find('input');
	var i, id;
	for(i=0;i<sons.length;i++){
		id = $(sons[i]).val().toLowerCase();
		if(id == "not playing"){
			id = "not_playing";
		}
		
		if(id == "on hold"){
			id = "onhold";
		}

		if($(sons[i]).is(":checked")){
			$('table.'+id).show()
			$('h2.'+id).show()
		}
		else{
			$('table.'+id).hide()
			$('h2.'+id).hide()
		}
	}
}
