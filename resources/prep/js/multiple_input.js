cur_selected = [];
updateCurSelectedArray();

cur_selected_pubs = [];
updateCurPubsSelected();


function updateCurPubsSelected(){
	var x = document.getElementById("cur_pubs");
	
	for(var i = 0; i < x.options.length; i++){
		if($.inArray(x.options[i].value, cur_selected_pubs) > -1){
		}
		else{
			cur_selected_pubs.push(x.options[i].value);
		}
	}
}

function add_pub_to_list(){
	var x = document.getElementById("publisher");
	
	for(var i = 0; i < x.options.length; i++){
		if(x.options[i].selected == true){
			if($.inArray(x.options[i].value, cur_selected_pubs) > -1){
			}
			else{
				add_to_current_pub_list(x.options[i].value,	x.options[i].innerHTML)
			}
		}
	}
}

function remove_pub(){
	var x = document.getElementById("cur_pubs")
	if (x.options.length > 0){
		var window=confirm("Are you sure you want to remove this publisher?");
		if (window==true){
			$("#cur_pubs option[value='"+x.options[x.selectedIndex].value+"']").remove();
		}
		else{
			return false;
		}
	}
}


function add_to_current_pub_list(id,label){
	var html = '';
	var x = document.getElementById("cur_pubs");
	
	for(var i = 0; i < x.options.length; i++){
		cur_selected_pubs.push(x.options[i].value)
		html += "<option value='" + x.options[i].value + "'>" + x.options[i].innerHTML + "</option>"
	}

	html += "<option value='" + id + "'>" + label + "</option>"
	
	$('#cur_pubs').html(html)
	
	updateCurPubsSelected()
}



function updateCurSelectedArray(){
	var x = document.getElementById("cur_developers");
	
	for(var i = 0; i < x.options.length; i++){
		if($.inArray(x.options[i].value, cur_selected) > -1){
		}
		else{
			cur_selected.push(x.options[i].value);
		}
	}
}

function add_dev_to_list(){
	var x = document.getElementById("developers");
	
	for(var i = 0; i < x.options.length; i++){
		if(x.options[i].selected == true){
			if($.inArray(x.options[i].value, cur_selected) > -1){
			}
			else{
				add_to_current_list(x.options[i].value,	x.options[i].innerHTML)
			}
		}
	}
}

function remove_dev(){
	var x = document.getElementById("cur_developers")
	if (x.options.length > 0){
		var window=confirm("Are you sure you want to remove this developer?")
		if (window==true){
			$("#cur_developers option[value='"+x.options[x.selectedIndex].value+"']").remove()
		}
		else{
			return false
		}
	}
}


function add_to_current_list(id,label){
	var html = '';
	var x = document.getElementById("cur_developers")
	
	for(var i = 0; i < x.options.length; i++){
		cur_selected.push(x.options[i].value)
		html += "<option value='" + x.options[i].value + "'>" + x.options[i].innerHTML + "</option>"
	}
	
	html += "<option value='" + id + "'>" + label + "</option>"
	
	$('#cur_developers').html(html)
	
	updateCurSelectedArray()
}

function multiForm_Select_ALL(id){
	$('#'+id+' option').prop('selected', 'selected');
}
