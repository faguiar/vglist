advsearch = false;

//HEADER SEARCH -- Submit form when ENTER is pressed inside input
$("#search_input").keyup(function(event){
	if (event.which === 13) {
		event.preventDefault();
        $(".header_search_form").submit();
	}  
});

//SEARCH PAGE ADVANCED SEARCH

function showAdvSearch(input){
	advsearch = true;

	//Hide all previous advanced search divs
	$('#advanced_search_game').hide();
	$('#advanced_search_platform').hide();
	
	var type = $('#adv_searchtype').val();
	
	if(type == 'game'){
		$('#advanced_search_game').show();
	}
	
	if(type == 'platform'){
		$('#advanced_search_platform').show();
	}
}