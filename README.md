My Shelf Project
====================

How to run:
-------------

Before proceding, please install the following packages. They may be available ate [pip](https://pypi.python.org/pypi/pip).

* [Django](https://www.djangoproject.com/)
* [South](http://south.aeracode.org/)
* [Django Extensions](http://django-extensions.readthedocs.org/en/latest/)
* [Tornado](http://www.tornadoweb.org/en/stable/)
* [Django Latency](https://pypi.python.org/pypi/django-latency/)
* [Python Imaging Library](http://www.pythonware.com/products/pil/)
* [Django Static Preprocess](https://bitbucket.org/beastarman/django-static-preprocess)
* [Unidecode](https://pypi.python.org/pypi/Unidecode)
