from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^run/(?P<hash_id>[a-zA-Z0-9]+)/$', 'action_confirm.views.run'),
    url(r'^cancel/(?P<hash_id>[a-zA-Z0-9]+)/$', 'action_confirm.views.cancel'),
)
