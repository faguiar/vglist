class BaseAction:
	def __init__(self):
		self.__redirect_url = '/'
	
	def set_redirect_url(self,url):
		self.__redirect_url = url
	
	def get_redirect_url(self):
		return self.__redirect_url
	
	def run(self,*args,**kwargs):
		raise NotImplementedError
	
	def cancel(self,*args,**kwargs):
		raise NotImplementedError
