from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.core.urlresolvers import reverse
import random
import string

from datetime import datetime
from django.utils import timezone

class ActionObject(models.Model):
	action = models.ForeignKey('Action')
	param_id = models.IntegerField(default=0)
	
	def get_obj(self):
		return None
	
	def save(self,*args,**kwargs):
		if(not self.param_id):
			param_max = ActionObject.objects.filter(action=self.action_id).aggregate(models.Max('param_id'))['param_id__max']
			self.param_id = param_max+1 if param_max else 1
		
		super(ActionObject,self).save(*args,**kwargs)
	
	class Meta:
		unique_together = [
			["action","param_id"]
		]
		
		index_together = [
			["action","param_id"]
		]
		
		ordering = ["action","param_id"]

class ActionObjectModel(ActionObject):
	content_type = models.ForeignKey(ContentType)
	object_id = models.CharField(max_length=100)
	content_object = generic.GenericForeignKey('content_type', 'object_id')
	
	def get_obj(self):
		return self.content_object

class ActionObjectString(ActionObject):
	str_value = models.CharField(max_length=128)
	
	def get_obj(self):
		return self.str_value

class Action(models.Model):
	module = models.CharField(max_length=128)
	action = models.CharField(max_length=128)
	
	public = models.BooleanField(default=False)
	
	hash_id = models.CharField(max_length=100,primary_key=True)
	
	creation_time = models.DateTimeField(default=timezone.now)
	expiration_time = models.DateTimeField(null=True,blank=True)
	
	def save(self, *args, **kwargs):
		if not self.pk:
			try:
				if(self.tte):
					self.expiration_time = self.creation_time + self.tte
			except AttributeError as e:
				pass
			while True:
				h = "".join([random.choice(string.letters + string.digits) for i in range(100)]);
				if(self.__class__.objects.filter(hash_id=h).count()==0):
					self.hash_id = h
					break
		super(self.__class__, self).save(*args, **kwargs)
	
	def run(self,delete_on_success=True):
		try:
			action = getattr(getattr(__import__('%s.actions' % (self.module)),'actions'),self.action)()
			if(self.expiration_time and (timezone.now() > self.expiration_time)):
				delete_on_success=True
			else:
				args = [i for i in self.actionobject_set.all()]
				
				for i in xrange(len(args)):
					try:
						args[i] = args[i].actionobjectmodel.get_obj()
					except ActionObjectModel.DoesNotExist:
						try:
							args[i] = args[i].actionobjectstring.get_obj()
						except ActionObjectString.DoesNotExist:
							args[i] = None
				action.run(*args)
		except NameError as e:
			raise e
		else:
			if(delete_on_success):
				self.delete()
			return action.get_redirect_url()
	
	def cancel(self,delete_on_success=True):
		try:
			action = getattr(getattr(__import__('%s.actions' % (self.module)),'actions'),self.action)()
			args = [i for i in self.actionobject_set.all()]
			
			for i in xrange(len(args)):
				try:
					args[i] = args[i].actionobjectmodel.get_obj()
				except ActionObjectModel.DoesNotExist:
					try:
						args[i] = args[i].actionobjectstring.get_obj()
					except ActionObjectString.DoesNotExist:
						args[i] = None
			action.cancel(*args)
		except Exception as e:
			raise e
		else:
			if(delete_on_success):
				self.delete()
			return action.get_redirect_url()
	
	def get_run_url(self):
		return reverse('action_confirm.views.run', kwargs={'hash_id':self.hash_id})
	
	def get_cancel_url(self):
		return reverse('action_confirm.views.cancel', kwargs={'hash_id':self.hash_id})
		
