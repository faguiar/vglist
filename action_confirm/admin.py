from django.contrib import admin
from action_confirm.models import *

class ActionAdmin(admin.ModelAdmin):
	exclude = ['hash_id','param_id']
	
	actions = ['run_actions','cancel_actions']
	
	def run_actions(self, request, queryset):
		errors = 0
		success = 0
		
		for i in queryset:
			try:
				i.run()
			except e:
				errors += 1
			else:
				success += 1
		
		self.message_user(request, "%s action%s successfully executed. %s action%s raised errors." % (success,'s' if success>=2 else '',errors,'s' if errors>=2 else ''))
	run_actions.short_description = "Run selected actions"
	
	def cancel_actions(self, request, queryset):
		errors = 0
		success = 0
		
		for i in queryset:
			try:
				i.cancel()
			except e:
				errors += 1
			else:
				success += 1
		
		self.message_user(request, "%s action%s successfully canceled. %s action%s raised errors." % (success,'s' if success>=2 else '',errors,'s' if errors>=2 else ''))
	cancel_actions.short_description = "Cancel selected actions"

admin.site.register(ActionObjectModel)
admin.site.register(Action,ActionAdmin)
