# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'ActionObjectModel', fields ['action', 'param_id']
        db.delete_unique(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])

        # Adding model 'ActionObjectString'
        db.create_table(u'action_confirm_actionobjectstring', (
            (u'actionobject_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['action_confirm.ActionObject'], unique=True, primary_key=True)),
            ('str_value', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'action_confirm', ['ActionObjectString'])

        # Adding model 'ActionObject'
        db.create_table(u'action_confirm_actionobject', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('action', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['action_confirm.Action'])),
            ('param_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'action_confirm', ['ActionObject'])

        # Adding unique constraint on 'ActionObject', fields ['action', 'param_id']
        db.create_unique(u'action_confirm_actionobject', ['action_id', 'param_id'])

        # Adding index on 'ActionObject', fields ['action', 'param_id']
        db.create_index(u'action_confirm_actionobject', ['action_id', 'param_id'])

        # Deleting field 'ActionObjectModel.param_id'
        db.delete_column(u'action_confirm_actionobjectmodel', 'param_id')

        # Deleting field 'ActionObjectModel.action'
        db.delete_column(u'action_confirm_actionobjectmodel', 'action_id')

        # Deleting field 'ActionObjectModel.id'
        db.delete_column(u'action_confirm_actionobjectmodel', u'id')

        # Adding field 'ActionObjectModel.actionobject_ptr'
        db.add_column(u'action_confirm_actionobjectmodel', u'actionobject_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(default='', to=orm['action_confirm.ActionObject'], unique=True, primary_key=True),
                      keep_default=False)

        # Removing index on 'ActionObjectModel', fields ['action', 'param_id']
        #db.delete_index(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])


    def backwards(self, orm):
        # Adding index on 'ActionObjectModel', fields ['action', 'param_id']
        #db.create_index(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])

        # Removing index on 'ActionObject', fields ['action', 'param_id']
        db.delete_index(u'action_confirm_actionobject', ['action_id', 'param_id'])

        # Removing unique constraint on 'ActionObject', fields ['action', 'param_id']
        db.delete_unique(u'action_confirm_actionobject', ['action_id', 'param_id'])

        # Deleting model 'ActionObjectString'
        db.delete_table(u'action_confirm_actionobjectstring')

        # Deleting model 'ActionObject'
        db.delete_table(u'action_confirm_actionobject')

        # Adding field 'ActionObjectModel.param_id'
        db.add_column(u'action_confirm_actionobjectmodel', 'param_id',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'ActionObjectModel.action'
        raise RuntimeError("Cannot reverse this migration. 'ActionObjectModel.action' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'ActionObjectModel.action'
        db.add_column(u'action_confirm_actionobjectmodel', 'action',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['action_confirm.Action']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'ActionObjectModel.id'
        raise RuntimeError("Cannot reverse this migration. 'ActionObjectModel.id' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'ActionObjectModel.id'
        db.add_column(u'action_confirm_actionobjectmodel', u'id',
                      self.gf('django.db.models.fields.AutoField')(primary_key=True),
                      keep_default=False)

        # Deleting field 'ActionObjectModel.actionobject_ptr'
        db.delete_column(u'action_confirm_actionobjectmodel', u'actionobject_ptr_id')

        # Adding unique constraint on 'ActionObjectModel', fields ['action', 'param_id']
        db.create_unique(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])


    models = {
        u'action_confirm.action': {
            'Meta': {'object_name': 'Action'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'expiration_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'}),
            'module': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'action_confirm.actionobject': {
            'Meta': {'ordering': "['action', 'param_id']", 'unique_together': "[['action', 'param_id']]", 'object_name': 'ActionObject', 'index_together': "[['action', 'param_id']]"},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['action_confirm.Action']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'param_id': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'action_confirm.actionobjectmodel': {
            'Meta': {'ordering': "['action', 'param_id']", 'object_name': 'ActionObjectModel', '_ormbases': [u'action_confirm.ActionObject']},
            u'actionobject_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['action_confirm.ActionObject']", 'unique': 'True', 'primary_key': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'action_confirm.actionobjectstring': {
            'Meta': {'ordering': "['action', 'param_id']", 'object_name': 'ActionObjectString', '_ormbases': [u'action_confirm.ActionObject']},
            u'actionobject_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['action_confirm.ActionObject']", 'unique': 'True', 'primary_key': 'True'}),
            'str_value': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['action_confirm']
