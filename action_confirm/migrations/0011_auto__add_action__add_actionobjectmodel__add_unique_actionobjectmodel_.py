# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Action'
        db.create_table(u'action_confirm_action', (
            ('module', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('action', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('public', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('hash_id', self.gf('django.db.models.fields.CharField')(max_length=100, primary_key=True)),
            ('creation_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('expiration_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'action_confirm', ['Action'])

        # Adding model 'ActionObjectModel'
        db.create_table(u'action_confirm_actionobjectmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('action', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['action_confirm.Action'])),
            ('param_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'action_confirm', ['ActionObjectModel'])

        # Adding unique constraint on 'ActionObjectModel', fields ['action', 'param_id']
        db.create_unique(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])

        # Adding index on 'ActionObjectModel', fields ['action', 'param_id']
        db.create_index(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])


    def backwards(self, orm):
        # Removing index on 'ActionObjectModel', fields ['action', 'param_id']
        db.delete_index(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])

        # Removing unique constraint on 'ActionObjectModel', fields ['action', 'param_id']
        db.delete_unique(u'action_confirm_actionobjectmodel', ['action_id', 'param_id'])

        # Deleting model 'Action'
        db.delete_table(u'action_confirm_action')

        # Deleting model 'ActionObjectModel'
        db.delete_table(u'action_confirm_actionobjectmodel')


    models = {
        u'action_confirm.action': {
            'Meta': {'object_name': 'Action'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'expiration_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'}),
            'module': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'action_confirm.actionobjectmodel': {
            'Meta': {'ordering': "['action', 'param_id']", 'unique_together': "[['action', 'param_id']]", 'object_name': 'ActionObjectModel', 'index_together': "[['action', 'param_id']]"},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['action_confirm.Action']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'param_id': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['action_confirm']