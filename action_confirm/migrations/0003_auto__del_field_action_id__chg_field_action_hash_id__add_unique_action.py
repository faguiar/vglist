# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Action.id'
        db.delete_column(u'action_confirm_action', u'id')


        # Changing field 'Action.hash_id'
        db.alter_column(u'action_confirm_action', 'hash_id', self.gf('django.db.models.fields.CharField')(max_length=100, primary_key=True))
        # Adding unique constraint on 'Action', fields ['hash_id']
        db.create_unique(u'action_confirm_action', ['hash_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Action', fields ['hash_id']
        db.delete_unique(u'action_confirm_action', ['hash_id'])


        # User chose to not deal with backwards NULL issues for 'Action.id'
        raise RuntimeError("Cannot reverse this migration. 'Action.id' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Action.id'
        db.add_column(u'action_confirm_action', u'id',
                      self.gf('django.db.models.fields.AutoField')(primary_key=True),
                      keep_default=False)


        # Changing field 'Action.hash_id'
        db.alter_column(u'action_confirm_action', 'hash_id', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        u'action_confirm.action': {
            'Meta': {'object_name': 'Action'},
            'func': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'}),
            'module': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'action_confirm.actionobjects': {
            'Meta': {'ordering': "['action', 'param_id']", 'unique_together': "[['action', 'param_id']]", 'object_name': 'ActionObjects', 'index_together': "[['action', 'param_id']]"},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['action_confirm.Action']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'param_id': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['action_confirm']