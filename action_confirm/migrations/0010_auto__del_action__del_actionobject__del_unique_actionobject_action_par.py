# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Action'
        db.delete_table(u'action_confirm_action')

        # Deleting model 'ActionObject'
        db.delete_table(u'action_confirm_actionobject')


    def backwards(self, orm):
        # Adding index on 'ActionObject', fields ['action', 'param_id']
        db.create_index(u'action_confirm_actionobject', ['action_id', 'param_id'])

        # Adding model 'Action'
        db.create_table(u'action_confirm_action', (
            ('creation_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('module', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('action', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('expiration_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('public', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('hash_id', self.gf('django.db.models.fields.CharField')(max_length=100, primary_key=True)),
        ))
        db.send_create_signal(u'action_confirm', ['Action'])

        # Adding model 'ActionObject'
        db.create_table(u'action_confirm_actionobject', (
            ('object_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('param_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('action', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['action_confirm.Action'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'action_confirm', ['ActionObject'])

        # Adding unique constraint on 'ActionObject', fields ['action', 'param_id']
        db.create_unique(u'action_confirm_actionobject', ['action_id', 'param_id'])


    models = {
        
    }

    complete_apps = ['action_confirm']
