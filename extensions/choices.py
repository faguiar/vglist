class IntegerChoices:
	def __init__(self,choices):
		self.c = choices
	
	def __getitem__(self,i):
		return self.c[i]
	
	def __len__(self):
		return len(self.c)
	
	def reverse_get(self,c):
		try:
			return self.c.index(c)
		except ValueError:
			return None
	
	def choices(self):
		return tuple([(i,self.c[i]) for i in xrange(len(self.c))])
	
	def __contains__(self,i):
		if(not isinstance(i,int)):
			return False
		else:
			return i>=0 and i<self.c
