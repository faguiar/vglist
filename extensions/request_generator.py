from django.test.client import RequestFactory
from django.contrib.sessions.backends.file import SessionStore
from django.contrib.auth.models import AnonymousUser

def generate_request():
	f = RequestFactory()
	request = f.request()
	request.session = SessionStore()
	request.user = AnonymousUser()
	
	return request
