DOMAIN_NAME = 'localhost:8000'

EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'

DEBUG = True

LATENCY_MIDDLEWARE = {
    'WAIT_TIME': 2.0
}
