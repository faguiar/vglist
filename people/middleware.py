from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from .require_perm import DoesNotHavePerm

class DoesNotHavePermHandler(object):
	def process_exception(self,request, exception):
		PEOPLE_NO_PERM_REDIRECT_VIEW = getattr(settings,'PEOPLE_NO_PERM_REDIRECT_VIEW',None)
		if(isinstance(exception,DoesNotHavePerm) and PEOPLE_NO_PERM_REDIRECT_VIEW):
			print getattr(settings,'PEOPLE_NO_PERM_REDIRECT_ARGS',[])
			return HttpResponseRedirect(
				reverse(
					PEOPLE_NO_PERM_REDIRECT_VIEW,
					args = getattr(settings,'PEOPLE_NO_PERM_REDIRECT_ARGS',[]),
					kwargs = getattr(settings,'PEOPLE_NO_PERM_REDIRECT_KWARGS',{})
				)
			)
		return None
