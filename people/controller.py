from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.core.mail import EmailMultiAlternatives
from django.template import loader, Context
from django.contrib.contenttypes.models import ContentType
from django.db import transaction

from people.models import *
from people import values
from action_confirm import models as action_confirm__models

from django.conf import settings
from django.core.files.base import File
from StringIO import StringIO

from extensions.controller import Controller

from datetime import timedelta

@transaction.atomic
def login(request,username,password):
	'''
	Return Codes:
		0. User logged in successfully with VGList account
			Returns people.User object
	
		1. User couldn't login
			Returns None
	
		2. User found, but inactive
			Returns people.User object
	'''
	
	user = authenticate(email=username, username=username.lower(), password=password)
	
	if(user):
		if(user.is_active):
			auth_login(request,user)
			return (0,user)
		else:
			return (2,user)
	else:
		return (1,None)

@transaction.atomic
def logout(request):
	'''
	Return Codes:
		0. User logged out
			Returns None
	'''
	
	auth_logout(request)
	return (0,None)

@transaction.atomic
def register(request,email,username,password):
	'''
	Return Codes:
		0. User created successfully
			Returns people.User object
	
		1. Email already exists
			Returns None
	
		2. Username already exists
			Returns None
	
		3. Email isn't valid
			Returns ValueError
	
		4. Username isn't valid
			Returns ValueError
	
		5. Password not secure
			Returns User.NotSecurePassword object
	'''
	
	username = username.lower()
	
	try:
		user = User.objects.create_user(email,username,password)
	except IntegrityError as e:
		if(e.args[0].split(' ')[1]=='email'):
			return (1,None)
		elif(e.args[0].split(' ')[1]=='username'):
			return (2,None)
	except ValueError as e:
		if(e.args[0].split(' ')[1]=='email'):
			return (3,None)
		elif(e.args[0].split(' ')[1]=='username'):
			return (4,None)
	except User.NotSecurePassword as e:
		return (5,e)
	else:
		action = action_confirm__models.Action.objects.create(
			module='people',
			action='Activate',
			public=True,
		)
		action_confirm__models.ActionObjectModel.objects.create(
			action=action,
			content_object=user,
		)
		
		c = Context({
				'domain': 'http://'+settings.DOMAIN_NAME,
				'user': user,
				'action': action,
		})
		
		msg = EmailMultiAlternatives('Registration Confirmation',loader.get_template('mail/registration_confirm.txt').render(c),'noreply@'+settings.DOMAIN_NAME,[user.email])
		msg.attach_alternative(loader.get_template('mail/registration_confirm.html').render(c), "text/html")
		msg.send()
		
		return (0,user)

@transaction.atomic
def resend_activation_email(request,user):
	'''
	Return Codes:
		0. Confirmation resent
			Returns action_confirm.Action object
	
		1. No action found. Creating and sending new Action confirmation email
			Returns new action_confirm.Action object
	
		2. User already confirmed
			Returns User object
	
		3. User not found
			Returns None
	'''
	
	if(not isinstance(user,User)):
		try:
			user = User.objects.get(email=user)
		except User.DoesNotExist:
			try:
				user = User.objects.get(username=user)
			except User.DoesNotExist:
				return (3,None)
	
	if(user.is_active):
		return (2,user)
	else:
		try:
			action_object = action_confirm__models.ActionObjectModel.objects.get(
				content_type__pk=ContentType.objects.get_for_model(user).pk,
				object_id=user.pk,
				action__module='people',
				action__action='Activate'
			)
		except action_confirm__models.ActionObjectModel.DoesNotExist as e:
			action = action_confirm__models.Action.objects.create(
				module='people',
				action='Activate',
				public=True,
			)
			action_confirm__models.ActionObjectModel.objects.create(
				action=action,
				content_object=user,
			)
			
			c = Context({
					'domain': 'http://'+settings.DOMAIN_NAME,
					'user': user,
					'action': action,
			})
			
			msg = EmailMultiAlternatives('Registration Confirmation',loader.get_template('mail/registration_confirm.txt').render(c),'noreply@'+settings.DOMAIN_NAME,[user.email])
			msg.attach_alternative(loader.get_template('mail/registration_confirm.html').render(c), "text/html")
			msg.send()
			
			return (1,action)
		else:
			c = Context({
					'domain': 'http://'+settings.DOMAIN_NAME,
					'user': user,
					'action': action_object.action,
			})
			
			msg = EmailMultiAlternatives('Registration Confirmation',loader.get_template('mail/registration_confirm.txt').render(c),'noreply@'+settings.DOMAIN_NAME,[user.email])
			msg.attach_alternative(loader.get_template('mail/registration_confirm.html').render(c), "text/html")
			msg.send()
			
			return (0,action_object.action)

@transaction.atomic
def edit_user(request,old_password=None,new_password=None,avatar=None,gender=None,email=None):
	'''
	Return Codes:
		0. User modified without errors
			Returns User
	
		1. User modified with some errors
			Returns (User,(field,error_code,extra_return)+)
				'new_password':
					1. New password is not secure
						Returns User.NotSecurePassword object
					2. Wrong Old Password
						Returns None
				'gender':
					1. Not valid value
						Returns None
				'avatar':
					1. Not valid image
						Returns None
				'email':
					1. Not valid email
						Returns None
					2. email already in use
						Returns None
	
		2. User not found
			Returns None
	'''
	
	user = request.user
	
	if(not user.is_authenticated()):
		return (2,None)
	
	errors = []
	
	if(new_password != None):
		if(user.check_password(old_password)):
			try:
				user.set_password(new_password, unsafe=False)
			except User.NotSecurePassword as e:
				errors.append(('new_password',1,e))
		else:
			errors.append(('new_password',2,None))
	
	if(gender != None):
		try:
			gender = int(gender)
		except ValueError:
			errors.append(('gender',1,None))
		else:
			if(gender in values.GENDER):
				user.gender = gender
			else:
				errors.append(('gender',1,None))
	
	if(avatar != None):
		if(isinstance(avatar,File) or isinstance(avatar,StringIO) or isinstance(avatar,file)):
			avatar = avatar.read()
		if(not user.save_images_from_raw(avatar)):
			errors.append(('avatar',1,None))
	
	if(email != None):
		if(not values.REGEX['email_validation'].match(email)):
			errors.append(('email',1,None))
		else:
			try:
				User.objects.get(email=email)
			except User.DoesNotExist:
				user.email = email
			else:
				errors.append(('email',2,None))
	
	user.save()
	
	if(len(errors)):
		return (1,user,errors)
	else:
		return (0,user,[])

class RequestRecoverPasswordController(Controller):
	STATUS_USER_FOUND = 1
	STATUS_NOT_USER_FOUND = 2
	
	def _create(self,user):
		if(not isinstance(user,User)):
			try:
				user = User.objects.get(email=user)
			except User.DoesNotExist:
				try:
					user = User.objects.get(username=user)
				except User.DoesNotExist:
					return RequestRecoverPasswordController.STATUS_NOT_USER_FOUND
		
		action = action_confirm__models.Action()
		action.module='people'
		action.action='ForgotPassword'
		action.public=True
		action.tte=timedelta(hours=24)
		action.save()
		
		action_confirm__models.ActionObjectModel.objects.create(
			action=action,
			content_object=user,
		)
		
		c = Context({
			'domain': 'http://'+settings.DOMAIN_NAME,
			'action': action,
		})
		
		msg = EmailMultiAlternatives('Recuperar Senha',loader.get_template('mail/forgot_password.txt').render(c),'noreply@'+settings.DOMAIN_NAME,[user.email])
		msg.attach_alternative(loader.get_template('mail/forgot_password.html').render(c), "text/html")
		msg.send()
		
		return RequestRecoverPasswordController.STATUS_USER_FOUND

class RecoverPasswordController(Controller):
	STATUS_REQUEST_FOUND = 1;
	STATUS_REQUEST_NOT_FOUND = 2;
	STATUS_REQUEST_EXPIRED = 3; # added  by becker not implemented in DB yet
	
	def _create(self,hash_id,password):
		try:
			action = action_confirm__models.Action.objects.get(hash_id=hash_id,module='people',action='CreateNewPassword');
		except action_confirm__models.Action.DoesNotExist:
			return RecoverPasswordController.STATUS_REQUEST_NOT_FOUND
		else:
			user = action.actionobject_set.all()[0].actionobjectmodel.get_obj()
			
			user.set_password(password);
			
			user.save()
			
			action.cancel();
			
			return RecoverPasswordController.STATUS_REQUEST_FOUND
		
		
	
