from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils import timezone

from PIL import Image
from StringIO import StringIO
from django.db.models.fields.files import ImageFieldFile, FileField
import os

from django.conf import settings
from people import values

class UserManager(BaseUserManager):
	def create_user(self,email,username,password):
		if(not values.REGEX['email_validation'].match(email)):
			raise ValueError('Invalid email')
		if(not values.REGEX['username_validation'].match(username)):
			raise ValueError('Invalid username')
		
		user = User.objects.create(email=email,username=username)
		try:
			user.set_password(password,False)
		except User.NotSecurePassword as e:
			user.delete()
			raise e
		
		user.save()
		
		return user
	
	def create_superuser(self, email, username, password):
		user = self.create_user(email,username,password)
		user.is_active = True
		user.is_admin=True
		user.save()
		return user

class User(AbstractBaseUser):
	AVATAR_SUBPATH = 'user/avatar/'
	
	class NotSecurePassword(Exception):
		MESSAGES = ["Password must have at least 6 characters"]
		
		def __init__(self,value=-1):
			self.value = value
		def __str__(self):
			return self.MESSAGES[self.value] if (self.value >= 0 and self.value < len(self.MESSAGES)) else ("Invalid error id " + str(self.value))
	
	email = models.EmailField(unique=True, db_index=True)
	
	is_active = models.BooleanField(default=False)
	is_admin = models.BooleanField(default=False)
	
	username = models.SlugField(max_length=32, primary_key=True)
	
	avatar = models.ImageField(upload_to=os.path.join(settings.MEDIA_ROOT,AVATAR_SUBPATH), default='', blank=True)
	
	join_time = models.DateTimeField(default=timezone.now)
	last_online = models.DateTimeField(default=timezone.now)
	
	gender = models.IntegerField(choices=values.GENDER.choices(), default=0)
	
	objects = UserManager()
	
	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []
	
	def thumbnail(self):
		if(self.avatar):
			filename = 't_%s' % (os.path.basename(self.avatar.name))
			path = os.path.join(os.path.dirname(self.avatar.name),'thumbs/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def save_images_from_raw(self,raw):
		filename = '%s.jpg' % (self.username)
		
		try:
			raw = Image.open(StringIO(raw)).convert("RGBA")
			img = Image.new("RGB",(raw.size[0],raw.size[1]),(255,255,255))
			img.paste(raw,(0,0),raw)
		except IOError:
			return False
		
		img.thumbnail((360,360),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(settings.MEDIA_ROOT,self.AVATAR_SUBPATH),filename), 'JPEG')
		self.avatar = os.path.join(self.AVATAR_SUBPATH,filename)
		
		img.thumbnail((80,80),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,self.AVATAR_SUBPATH),'thumbs/'),'t_%s' % (filename)), 'JPEG')
		
		return True
	
	def get_full_name(self):
		return self.email
	
	def get_short_name(self):
		return self.email
	
	def has_perm(self, perm, obj=None):
		return True
	
	def has_module_perms(self, app_label):
		return True
	
	def secure_password(self, raw_password):
		if(len(raw_password)<6):
			raise self.NotSecurePassword(0)
	
	def set_password(self, raw_password, unsafe=True):
		if(not unsafe):
			self.secure_password(raw_password)
		super(User, self).set_password(raw_password)
	
	def has_custom_perm(self,perm):
		groups = self.groupmember_set.all()
		perms = getattr(settings,"USER_GROUPS_PERMISSIONS",{})
		
		for g in groups:
			if (g.group in perms and perm in perms[g.group]):
				return True
		
		return False
	
	@property
	def is_staff(self):
		return self.is_admin
	
	def __unicode__(self):
		return self.username

class Comment(models.Model):
	text = models.TextField()
	post_time = models.DateTimeField(default=timezone.now)
	
	def __unicode__(self):
		if(len(self.text)>53):
			return self.text[:50]+"..."
		else:
			return self.text

class GroupMember(models.Model):
	user = models.ForeignKey("User")
	group = models.CharField(max_length=16)
	
	class Meta:
		unique_together = (('user','group'),)
		index_together = (('user','group'),)
