from django.core.handlers.wsgi import WSGIRequest
import functools
from types import FunctionType as function

class DoesNotHavePerm(Exception):
	def __init__(self,perm):
		self.perm = perm
		Exception.__init__(self, "User has not the following permission: %s" % (perm,))

def requirePerm(perm):
	def can(request):
		return testPerm(request,perm)
	
	def testAndCall(callback):
		def call(self, request,*args,**kwargs):
			if(not can(request)):
				raise DoesNotHavePerm(perm)
			callback(self,request,*args,**kwargs)
		return call
	
	def changeIt(func):
		
		if(isinstance(func,function)):
			func.can = can 
		else:
			func.can = staticmethod(can)
		
		func.__call__ = testAndCall(func.__call__)
		func.__init__ = testAndCall(func.__init__)
		return func
	
	return changeIt

def testPerm(user,perm):
	if(isinstance(user,WSGIRequest)):
		user = user.user
				
	if(perm):
		return user.is_authenticated() and (user.is_admin or user.has_custom_perm(perm))
	else:
		return True
