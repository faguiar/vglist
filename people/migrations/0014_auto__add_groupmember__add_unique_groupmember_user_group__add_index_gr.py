# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GroupMember'
        db.create_table(u'people_groupmember', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.User'])),
            ('group', self.gf('django.db.models.fields.CharField')(max_length=16)),
        ))
        db.send_create_signal(u'people', ['GroupMember'])

        # Adding unique constraint on 'GroupMember', fields ['user', 'group']
        db.create_unique(u'people_groupmember', ['user_id', 'group'])

        # Adding index on 'GroupMember', fields ['user', 'group']
        db.create_index(u'people_groupmember', ['user_id', 'group'])


    def backwards(self, orm):
        # Removing index on 'GroupMember', fields ['user', 'group']
        db.delete_index(u'people_groupmember', ['user_id', 'group'])

        # Removing unique constraint on 'GroupMember', fields ['user', 'group']
        db.delete_unique(u'people_groupmember', ['user_id', 'group'])

        # Deleting model 'GroupMember'
        db.delete_table(u'people_groupmember')


    models = {
        u'people.comment': {
            'Meta': {'object_name': 'Comment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'people.groupmember': {
            'Meta': {'unique_together': "(('user', 'group'),)", 'object_name': 'GroupMember', 'index_together': "(('user', 'group'),)"},
            'group': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.User']"})
        },
        u'people.user': {
            'Meta': {'object_name': 'User'},
            'avatar': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'join_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_online': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.SlugField', [], {'max_length': '32', 'primary_key': 'True'})
        }
    }

    complete_apps = ['people']