import re

from extensions.choices import IntegerChoices

REGEX = {
	'email_validation': r'^[^@]+@[^@]+\.[^@]+$',
	'username_validation': r'^[a-zA-Z0-9_.-]+$',
}

for i in REGEX:
	REGEX[i] = re.compile(REGEX[i])

GENDER = IntegerChoices([
	'Not Set',
	'Male',
	'Female',
	'Other',
])
