from action_confirm import actions
from django.core.urlresolvers import reverse

from people.controller import *

class Activate(actions.BaseAction):
	def run(self,user):
		user.is_active = True
		user.save()
		
		self.set_redirect_url(reverse('vglist.views.login')+'?msg=activated')
	
	def cancel(self,user):
		user.delete()
		
		self.set_redirect_url(reverse('vglist.views.home')+'?msg=activation_canceled')

class ForgotPassword(actions.BaseAction):
	def run(self,user):
		action = action_confirm__models.Action()
		action.module='people'
		action.action='CreateNewPassword'
		action.public=False
		action.tte=timedelta(hours=1)
		action.save()
		
		action_confirm__models.ActionObjectModel.objects.create(
			action=action,
			content_object=user,
		)
		
		self.set_redirect_url(reverse('vglist.views.recover_password')+"?hash="+action.hash_id)
	
	def cancel(self,user):
		self.set_redirect_url(reverse('vglist.views.home'))

class CreateNewPassword(actions.BaseAction):
	def cancel(self,user):
		self.set_redirect_url(reverse('vglist.views.home'))
