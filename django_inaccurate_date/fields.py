from django.db import models
from django.utils.six import with_metaclass
from south.modelsinspector import add_introspection_rules
from django import forms

from datetime import date
import re

add_introspection_rules([], ["^django_inaccurate_date\.fields\.InaccurateDateField"])

class InaccurateDate:
	MONTH_DETECT_RE = re.compile("%[bBm]")
	DAY_DETECT_RE = re.compile("%[aAwdjUWcx]")
	REMOVE_PERCENT = re.compile("%%")
	
	def __init__(self, year, month=0, day=0):
		if(not year):
			raise ValueError("Year cannot be null")
		if(day and not month):
			raise ValueError("Month cannot be null if day isn't as well")
		if(day):
			date(year,month,day)
		elif(month):
			date(year,month,1)
		else:
			date(year,1,1)
		self.year = year
		self.month = month
		self.day = day
	
	def __eq__(self, other):
		return (isinstance(other, self.__class__)
			and self.__dict__ == other.__dict__)

	def __ne__(self, other):
		return not self.__eq__(other)
		
	@staticmethod
	def create_from_date(date_object):
		return InaccurateDate(date_object.year, date_object.month, date_object.day)
	
	def date(self):
		return date(self.year, self.month if self.month else 1, self.day if self.day else 1)
	
	def strftime(self,format):
		clean = self.REMOVE_PERCENT.sub("_",format)
		
		if(not self.day and self.DAY_DETECT_RE.search(clean)):
			raise ValueError("I can't print a day if I don't have it")
		if(not self.month and self.MONTH_DETECT_RE.search(clean)):
			raise ValueError("I can't print a month if I don't have it")
		
		return self.date().strftime(format)

class InaccurateDateFormWidget(forms.MultiWidget):
	def __init__(self,attrs={}):
		attrs.update({'style': 'width:80px;margin-right:20px;'})
		
		widgets=(
			forms.NumberInput(attrs=attrs),
			forms.Select(attrs=attrs, choices=(
				('00',"Unknown"),
				('01',"January"),
				('02',"February"),
				('03',"March"),
				('04',"Abril"),
				('05',"May"),
				('06',"June"),
				('07',"July"),
				('08',"August"),
				('09',"September"),
				('10',"October"),
				('11',"November"),
				('12',"December"),
			)),
			forms.Select(attrs=attrs, choices=(
				('00',"Unknown"),
				('01',"1"),
				('02',"2"),
				('03',"3"),
				('04',"4"),
				('05',"5"),
				('06',"6"),
				('07',"7"),
				('08',"8"),
				('09',"9"),
				('10',"10"),
				('11',"11"),
				('12',"12"),
				('13',"13"),
				('14',"14"),
				('15',"15"),
				('16',"16"),
				('17',"17"),
				('18',"18"),
				('19',"19"),
				('20',"20"),
				('21',"21"),
				('22',"22"),
				('23',"23"),
				('24',"24"),
				('25',"25"),
				('26',"26"),
				('27',"27"),
				('28',"28"),
				('29',"29"),
				('30',"30"),
				('31',"31"),
			)),
		)
		super(InaccurateDateFormWidget, self).__init__(widgets, attrs)
	
	def decompress(self,value):
		if(value):
			return [
				"%04d" % value.year, 
				"%02d" % value.month,
				"%02d" % value.day
			]
		return ['','','']

class InaccurateDateFormField(forms.MultiValueField):
	widget = InaccurateDateFormWidget
	
	def __init__(self, *args, **kwargs):
		fields = (
			forms.IntegerField(),
			forms.IntegerField(),
			forms.IntegerField(),
		)
		
		super(InaccurateDateFormField,self).__init__(fields,*args,**kwargs)
	
	def compress(self,data_list):
		if(data_list[0]):
			return '%04d-%02d-%02d' % (int(data_list[0]),int(data_list[1]),int(data_list[2]))
		else:
			return ''

class InaccurateDateField(with_metaclass(models.SubfieldBase, models.Field)):
	description = "Inaccurate dates"
	DATETIME_RE = re.compile("^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})$")
	
	def __init__(self,*args,**kwargs):
		super(InaccurateDateField,self).__init__(*args,**kwargs)
	
	def get_value(self,value):
		if(not value):
			return None
		if(isinstance(value,date)):
			return InaccurateDate.create_from_date(value)
		elif(isinstance(value,str) or isinstance(value,unicode)):
			m = self.DATETIME_RE.match(value)
			if(m):
				return InaccurateDate(int(m.group("year")),int(m.group("month")),int(m.group("day")))
			else:
				raise ValueError("string must be in format YYYY-MM-DD")
		return value
	
	def db_type(self, connection):
		return 'char(10)'
	
	def to_python(self,value):
		return self.get_value(value)
	
	def get_prep_value(self, value):
		d = self.get_value(value)
		if(d):
			return "%04d-%02d-%02d" % (d.year, d.month, d.day)
		else:
			return None
	
	def formfield(self,**kwargs):
		defaults = {'form_class': InaccurateDateFormField}
		defaults.update(kwargs)
		return super(InaccurateDateField, self).formfield(**defaults)
	
	def get_internal_type(self):
		return 'CharField'
	
	def value_to_string(self, obj):
		value = self._get_val_from_obj(obj)
		return self.get_prep_value(value)
