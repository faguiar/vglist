from django import template
from datetime import date
from django_inaccurate_date.fields import InaccurateDate
 
register = template.Library()

class InaccurateDateNode(template.Node):
	
	def __init__(self, var, formats, ass=None):
		self.var = var
		self.formats = formats
		self.ass = ass
	
	def format(self, context):
		try:
			d = template.Variable(self.var).resolve(context)
			if(isinstance(d,str) or isinstance(d,unicode)):
				return unicode(d)
			if(not (isinstance(d,date) or isinstance(d,InaccurateDate))):
				raise template.VariableDoesNotExist("")
		except template.VariableDoesNotExist:
			return u""
		
		for f in self.formats:
			try:
				_f = template.Variable(f).resolve(context)
				return unicode(d.strftime(_f))
			except:
				pass
		
		return u""
	
	def render(self, context):
		value = self.format(context)
		if(self.ass):
			context[self.ass] = value
			return u""
		else:
			return value

def inaccurate_date(parser, token):
	"""
		{% inaccurate_date <var_name> <format>+ [ as <target_var_name> ] %}
	"""
	parts = token.split_contents()
	
	if(len(parts) < 3):
		raise template.TemplateSyntaxError("'inaccurate_date' tag must be of the form:  {% inaccurate_date <var_name> <format>+ [ as <target_var_name> ] %}")
	
	if(len(parts) >= 5 and parts[-2] == "as"):
		return InaccurateDateNode(parts[1],parts[2:-2],parts[-1])
	else:
		return InaccurateDateNode(parts[1],parts[2:])

register.tag('inaccurate_date', inaccurate_date)
