from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.db import transaction
from django.db import IntegrityError
from django.utils import timezone
from django.db.models import Min

from decimal import Decimal
from math import floor
from urlparse import urlparse
from unidecode import unidecode

from StringIO import StringIO
from django.core.files.base import File
from datetime import datetime, date, timedelta

from people import controller
from people.require_perm import requirePerm,testPerm

from django_inaccurate_date.fields import *

import people
import extensions.models
from vglist.models import *
from vglist.values import *

from extensions.controller import Controller,SearchController,assertStatus,desertStatus

@requirePerm(None)
@transaction.atomic
def get_game(request,game):
	'''
	Return Codes:
		0. Game Found
			Returns GameLocal object
	
		1. Game not found
			Returns None
		
		2. Null game
			Returns None
	'''
	
	if(not isinstance(game,GameLocal)):
		try:
			iter(game)
		except TypeError:
			return (1,None)
		else:
			if(len(game) != 2):
				return (1,None)
			try:
				game = (int(game[0]),int(game[1]))
				
				if(game[0] == 0 and game[1] == 0):
					return (2,None)
				
				game = GameLocal.objects.get(game_global=game[0], local_id=game[1])
			except (GameLocal.DoesNotExist, ValueError) as e:
				return (1,None)
	
	return (0,game)

@requirePerm(None)
@transaction.atomic
def get_game_global(request,game):
	'''
	Return Codes:
		0. Game Found
			Returns GameLocal object
	
		1. Game not found
			Returns None
		
		2. Null game
			Returns None
	'''
	
	if(not isinstance(game,GameGlobal)):
		if(not isinstance(game,GameLocal)):
			try:
				game = int(game)
				if(game == 0):
					return (2,None)
				game = GameGlobal.objects.get(pk=game)
			except (GameGlobal.DoesNotExist, ValueError) as e:
				return (1,None)
		else:
			game = game.game_global
	
	return (0,game)

@requirePerm(None)
@transaction.atomic
def get_studio(request,studio):
	'''
	Return Codes:
		0. Studio Found
			Returns Studio object
	
		1. Studio not found
			Returns None
		
		2. Null studio
			Returns None
	'''
	
	if(not isinstance(studio,Studio)):
		try:
			studio = int(studio)
			if(studio == 0):
				return (2,None)
			studio = Studio.objects.get(pk=studio)
		except (Studio.DoesNotExist, ValueError) as e:
			return (1,None)
	
	return (0,studio)

@requirePerm(None)
@transaction.atomic
def get_platform(request,platform):
	'''
	Return Codes:
		0. Platform Found
			Returns Platform object
	
		1. Platform not found
			Returns None
		
		2. Null platform
			Returns None
	'''
	
	if(not isinstance(platform,Platform)):
		try:
			platform = int(platform)
			if(platform == 0):
				return (2,None)
			platform = Platform.objects.get(pk=platform)
		except (Platform.DoesNotExist, ValueError) as e:
			return (1,None)
	
	return (0,platform)

@requirePerm(None)
@transaction.atomic
def get_vglistuser(request,vglistuser):
	'''
	Return Codes:
		0. VGListUser Found
			Returns VGListUser object
	
		1. VGListUser not found
			Returns None
		
		2. Null VGListUser
			Returns None
	'''
	
	if(not isinstance(vglistuser,VGListUser)):
		try:
			if(vglistuser == ""):
				return (2,None)
			vglistuser = VGListUser.objects.get(pk=vglistuser.lower())
		except VGListUser.DoesNotExist:
			return (1,None)
	
	return (0,vglistuser)

@requirePerm(None)
@transaction.atomic
def get_logged_vglistuser(request):
	'''
	Return Codes:
		0. VGListUser logged in
			Returns VGListUser object
	
		1. User not logged in
			Returns None
	
		2. User logged in, but has no VGList account:
			Returns None
	'''
	
	if(not request.user.is_authenticated()):
		return (1,None)
	
	try:
		user = request.user.vglistuser
	except VGListUser.DoesNotExist:
		return (2,None)
	
	return (0,user)

@requirePerm(None)
@transaction.atomic
def get_list_entry(request,user,game):
	'''
		Return Codes:
			0. ListEntry found:
				Returns ListEntry
			
			1. ListEntry not found:
				Returns None
	'''
	
	user = get_vglistuser(request,user)
	
	if(user[0] != 0):
		return (1,None)
	else:
		user = user[1]
	
	game = get_game(request,game)
	
	if(game[0] != 0):
		return (1,None)
	else:
		game = game[1]
	
	try:
		listentry = ListEntry.objects.get(user=user, game=game)
	except ListEntry.DoesNotExist:
		return (1,None)
	
	return (0,listentry)

@requirePerm(None)
@transaction.atomic
def login(request,username,password):
	'''
	Return Codes:
		0. User logged in sucessfuly with VGList account
			Returns people.User object
	
		1. User couldn't login
			Returns None
	
		2. User found, but inactive
			Returns people.User object
	
		3. User logged in sucessfuly without VGList account
			Returns people.User object
	'''
	
	res = controller.login(request,username,password)
	
	if(res[0]==0):
		try:
			res[1].vglistuser
		except VGListUser.DoesNotExist as e:
			return (3,res[1])
		else:
			return res
	else:
		return res

@requirePerm(None)
@transaction.atomic
def register(request,email,username,password,can_receive_mail=None):
	'''
	Return Codes:
		0. User created successfully
			Returns people.User object
	
		1. User already exists
			Returns None
	
		2. At least one of the required fields isn't available
			Returns ValueError
	
		3. Password not secure
			Returns User.NotSecurePassword object
	
		4. Username already exists
			Returns None
	'''
	
	username = username.lower()
	
	if(username == "me"):
		return (4,None)
	
	res = controller.register(request,email,username,password)
	
	if(res[0]==0):
		v = VGListUser.objects.create(user=res[1])
		
		try:
			if(can_receive_mail != None):
				v.can_receive_mail = bool(can_receive_mail)
				v.save()
		except:
			pass
	
	return res

@requirePerm(None)
@transaction.atomic
def add_comment_to_studio(request,studio,comment_text):
	'''
	Return Codes:
		0. Comment created successfully
			Returns VGListComment object
	
		1. User not logged in
			Returns None
	
		2. User doesn't have an VGList account
			Returns None
	
		3. Studio not found
			Returns None
		
		4. Comment has no body
			Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (1,None)
	elif(user[0]==2):
		return (2,None)
	else:
		user = user[1]
	
	studio = get_studio(request,studio)
	
	if(studio[0]!= 0):
		return(3,None)
	else:
		studio = studio[1]
	
	comment_text = comment_text.strip()
	
	if(len(comment_text) == 0):
		return (4,None)
	
	return(0,VGListComment.objects.create(
		comment = people.models.Comment.objects.create(
			text = comment_text,
		),
		commenter = user,
		content_object = studio,
	))

@requirePerm(None)
@transaction.atomic
def add_comment_to_platform(request,platform,comment_text):
	'''
	Return Codes:
		0. Comment created successfully
			Returns VGListComment object
	
		1. User not logged in
			Returns None
	
		2. User doesn't have an VGList account
			Returns None
	
		3. Platform not found
			Returns None
		
		4. Comment has no body
			Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (1,None)
	elif(user[0]==2):
		return (2,None)
	else:
		user = user[1]
	
	platform = get_platform(request,platform)
	
	if(platform[0]!= 0):
		return(3,None)
	else:
		platform = platform[1]
	
	comment_text = comment_text.strip()
	
	if(len(comment_text) == 0):
		return (4,None)
	
	return(0,VGListComment.objects.create(
		comment = people.models.Comment.objects.create(
			text = comment_text,
		),
		commenter = user,
		content_object = platform,
	))

@requirePerm(None)
@transaction.atomic
def add_comment_to_profile(request,profile_user,comment_text):
	'''
	Return Codes:
		0. Comment created successfully
			Returns VGListComment object
	
		1. User not logged in
			Returns None
	
		2. User doesn't have an VGList account
			Returns None
	
		3. VGListUser not found
			Returns None
		
		4. Comment has no body
			Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (1,None)
	elif(user[0]==2):
		return (2,None)
	else:
		user = user[1]
	
	profile_user = get_vglistuser(request,profile_user)
	
	if(profile_user[0] != 0):
		return (3,None)
	else:
		profile_user = profile_user[1]
	
	comment_text = comment_text.strip()
	
	if(len(comment_text) == 0):
		return (4,None)
	
	return(0,VGListComment.objects.create(
		comment = people.models.Comment.objects.create(
			text = comment_text,
		),
		commenter = user,
		content_object = profile_user,
	))

@requirePerm(None)
@transaction.atomic
def add_comment_to_game(request,game,comment_text):
	'''
	Return Codes:
		0. Comment created successfully
			Returns VGListComment object
	
		1. User not logged in
			Returns None
	
		2. User doesn't have an VGList account
			Returns None
	
		3. GameLocal not found
			Returns None
		
		4. Comment has no body
			Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (1,None)
	elif(user[0]==2):
		return (2,None)
	else:
		user = user[1]
	
	game = get_game(request,game)
	
	if(game[0] != 0):
		return (3,None)
	else:
		game = game[1]
	
	comment_text = comment_text.strip()
	
	if(len(comment_text) == 0):
		return (4,None)
	
	return(0,VGListComment.objects.create(
		comment = people.models.Comment.objects.create(
			text = comment_text,
		),
		commenter = user,
		content_object = game,
	))

@requirePerm(None)
@transaction.atomic
def edit_user(request,old_password=None,new_password=None,avatar=None,gender=None,email=None,bio=None,can_receive_mail=None,banner=None):
	'''
	Return Codes:
		0. User modified without errors
			Returns User
	
		1. User modified with some errors
			Returns (User,(field,error_code,extra_return)+)
				'new_password':
					1. New password is not secure
						Returns User.NotSecurePassword object
					2. Wrong Old Password
						Returns None
				'gender':
					1. Not valid value
						Returns None
				'avatar':
					1. Not valid image
						Returns None
				'email':
					1. Not valid email
						Returns None
					2. email already in use
						Returns None
	
		2. User not found
			Returns None
	
		3. User doesn`t have an VGList account
			Returns None
	'''
	
	try:
		with transaction.atomic():
			res = controller.edit_user(request,old_password,new_password,avatar,gender,email)
			if(res[0]==0 or res[0]==1):
				user = res[1].vglistuser
				
				user.bio = bio if bio else ''
				
				try:
					if(can_receive_mail != None):
						user.can_receive_mail = bool(can_receive_mail)
				except:
					pass
				
				try:
					if(banner != None):
						if(isinstance(banner,File) or isinstance(banner,StringIO) or isinstance(banner,file)):
							banner = banner.read()
						user.save_banner_from_raw(banner)
				except:
					pass
				
				user.save()
				
				return res
			else:
				return res
	except VGListUser.DoesNotExist:
		return (3,None)

@requirePerm("add")
@transaction.atomic
def create_new_game(
		request,
		name,
		local_name=None,
		synopsis=None,
		synopsis_source_name=None,
		synopsis_source_link=None,
		release_date=None,
		publishers=None,
		add_publishers=None,
		remove_publishers=None,
		developers=None,
		add_developers=None,
		remove_developers=None,
		platform=None,
		boxart=None,
		**kwars):
	'''
		Return Codes:
			0. Game created successfully:
				Returns GameLocal object
			
			1. Game created with some errors:
				Returns (GameLocal,(field,error_code,extra_return)+)
					'release_date':
						1. Date is not valid
							Returns None
					'synopsis_source_link':
						1. Not an valid URL
							Returns None
					'publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'add_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'remove_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher removed successfully:
									Returns None
								1. publisher not found:
									Returns None
					'platform':
						1. Platform not found
							Returns None
					'developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'add_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'remove_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer removed successfully:
									Returns None
								1. Developer not found:
									Returns None
					'boxart':
						1. Image not valid
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
			
			4. GameGlobal not created. Name empty:
				Returns None
	'''
	
	errors = []
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	if(not name):
		return (4,None)
	
	if(synopsis == None):
		synopsis = ''
	
	if(not local_name):
		local_name = name
	
	if(not synopsis_source_name):
		synopsis_source_name = ""
	
	if(not synopsis_source_link):
		synopsis_source_link = ""
	else:
		if(not synopsis_source_link.startswith(("http://","https://"))):
			synopsis_source_link = "http://" + synopsis_source_link
		synopsis_source_link = urlparse(synopsis_source_link)
		if(not ('.' in synopsis_source_link.netloc)):
			errors.append(('synopsis_source_link',1,None))
		else:
			synopsis_source_link = synopsis_source_link.geturl()
	
	game_global = GameGlobal.objects.create(
		name = name,
		synopsis = synopsis,
		synopsis_source_name = synopsis_source_name,
		synopsis_source_link = synopsis_source_link,
	)
	
	res = create_new_game_version(request,game_global,
		local_name = local_name,
		release_date = release_date,
		publishers = publishers,
		add_publishers = add_publishers,
		remove_publishers = remove_publishers,
		developers = developers,
		add_developers = add_developers,
		remove_developers = remove_developers,
		platform = platform,
		boxart = boxart,
	)
	
	if(len(errors)==0):
		return res
	else:
		if(res[0]==0):
			return (1,res[1],errors)
		else:
			return (1,res[1],res[2]+errors)

@requirePerm("add")
@transaction.atomic
def create_new_game_version(
		request,
		game,
		local_name,
		release_date=None,
		publishers=None,
		add_publishers=None,
		remove_publishers=None,
		developers=None,
		add_developers=None,
		remove_developers=None,
		platform=None,
		boxart=None,
		**kwargs):
	'''
		Return Codes:
			0. Game created successfully:
				Returns GameLocal object
			
			1. Game created with some errors:
				Returns (GameLocal,(field,error_code,extra_return)+)
					'release_date':
						1. Date is not valid
							Returns None
					'publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'add_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'remove_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher removed successfully:
									Returns None
								1. publisher not found:
									Returns None
					'platform':
						1. Platform not found
							Returns None
					'developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'add_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'remove_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer removed successfully:
									Returns None
								1. Developer not found:
									Returns None
					'boxart':
						1. Image not valid
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
				
			4. GameGlobal not found:
				Returns None
			
			5. GameLocal not created. Name empty:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	game = get_game_global(request,game)
	
	if(game[0]!=0):
		return (4,None)
	
	game = game[1]
	
	if(not local_name):
		return (5,None)
	
	game = GameLocal.objects.create(
		game_global = game,
		name = local_name,
	)
	
	return edit_game(request,game,None,local_name,None,
		release_date = release_date,
		publishers = publishers,
		add_publishers = add_publishers,
		remove_publishers = remove_publishers,
		developers = developers,
		add_developers = add_developers,
		remove_developers = remove_developers,
		platform = platform,
		boxart = boxart,
	)

@requirePerm("add")
@transaction.atomic
def edit_game(
		request,
		game,
		name=None,
		local_name=None,
		synopsis=None,
		synopsis_source_name=None,
		synopsis_source_link=None,
		release_date=None,
		publishers=None,
		add_publishers=None,
		remove_publishers=None,
		developers=None,
		add_developers=None,
		remove_developers=None,
		platform=None,
		boxart=None,
		**kwargs):
	'''
		Return Codes:
			0. Game created successfully:
				Returns GameLocal object
			
			1. Game created with some errors:
				Returns (GameLocal,(field,error_code,extra_return)+)
					'name':
						1. Name is empty
							Returns None
					'local_name':
						1. Local name is empty
							Returns None
					'release_date':
						1. Date is not valid
							Returns None
					'synopsis_source_link':
						1. Not an valid URL
							Returns None
					'publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'add_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'remove_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher removed successfully:
									Returns None
								1. publisher not found:
									Returns None
					'platform':
						1. Platform not found
							Returns None
					'developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'add_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'remove_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer removed successfully:
									Returns None
								1. Developer not found:
									Returns None
					'boxart':
						1. Image not valid
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
			
			4. Game not found:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	game = get_game(request,game)
	
	if(game[0]!=0):
		return (4,None)
	
	game = game[1]
	
	errors = []
	
	if(name != None):
		if(not name):
			errors.append(('name',1,None))
		else:
			game.game_global.name = name
	
	if(synopsis != None):
		game.game_global.synopsis = synopsis
	
	if(synopsis_source_name != None):
		game.game_global.synopsis_source_name = synopsis_source_name
	
	if(synopsis_source_link != None):
		if(not synopsis_source_link.startswith(("http://","https://"))):
			synopsis_source_link = "http://" + synopsis_source_link
		synopsis_source_link = urlparse(synopsis_source_link)
		if(not ('.' in synopsis_source_link.netloc)):
			errors.append(('synopsis_source_link',1,None))
		else:
			game.game_global.synopsis_source_link = synopsis_source_link.geturl()
	
	if(local_name != None):
		if(not local_name):
			errors.append(('local_name',1,None))
		else:
			game.name = local_name
	
	if(release_date != None):
		if(isinstance(release_date,date)):
			game.release_date = release_date
		elif(not release_date or release_date == "0000-00-00"):
			game.release_date = None
		else:
			try:
				game.release_date = InaccurateDateField().get_value(release_date)
			except:
				errors.append(('release_date',1,None))
	
	if(publishers != None):
		publishers_ret = []
		ok=0
		try:
			iter(publishers)
		except TypeError:
			publishers = [publishers]
		
		game.publishers.clear()
		
		for dev in publishers:
			if(not isinstance(dev,Studio)):
				try:
					dev = Studio.objects.get(pk=dev)
				except Studio.DoesNotExist:
					publishers_ret.append((1,None))
					ok=1
					continue
			game.publishers.add(dev)
			publishers_ret.append((0,None))
		
		if(ok):
			errors.append(('publishers',ok,publishers_ret))
	else:
		if(add_publishers != None):
			add_publishers_ret = []
			ok=0
			try:
				iter(add_publishers)
			except TypeError:
				add_publishers = [add_publishers]
			
			for dev in add_publishers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						add_publishers_ret.append((1,None))
						ok=1
						continue
				game.publishers.add(dev)
				add_publishers_ret.append((0,None))
			
			if(ok):
				errors.append(('add_publishers',ok,add_publishers_ret))
		
		if(remove_publishers != None):
			remove_publishers_ret = []
			ok=0
			try:
				iter(remove_publishers)
			except TypeError:
				remove_publishers = [remove_publishers]
			
			for dev in remove_publishers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						remove_publishers_ret.append((1,None))
						ok=1
						continue
				game.publishers.remove(dev)
				remove_publishers_ret.append((0,None))
			
			if(ok):
				errors.append(('remove_publishers',ok,remove_publishers_ret))
	
	if(platform != None):
		if(isinstance(platform,Platform)):
			game.console = platform
		else:
			platform = get_platform(request,platform)
			if(platform[0] == 1):
				errors.append(('platform',1,None))
			else:
				game.console = platform[1]
	
	if(developers != None):
		developers_ret = []
		ok=0
		try:
			iter(developers)
		except TypeError:
			developers = [developers]
		
		game.developers.clear()
		
		for dev in developers:
			if(not isinstance(dev,Studio)):
				try:
					dev = Studio.objects.get(pk=dev)
				except Studio.DoesNotExist:
					developers_ret.append((1,None))
					ok=1
					continue
			game.developers.add(dev)
			developers_ret.append((0,None))
		
		if(ok):
			errors.append(('developers',ok,developers_ret))
	else:
		if(add_developers != None):
			add_developers_ret = []
			ok=0
			try:
				iter(add_developers)
			except TypeError:
				add_developers = [add_developers]
			
			for dev in add_developers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						add_developers_ret.append((1,None))
						ok=1
						continue
				game.developers.add(dev)
				add_developers_ret.append((0,None))
			
			if(ok):
				errors.append(('add_developers',ok,add_developers_ret))
		
		if(remove_developers != None):
			remove_developers_ret = []
			ok=0
			try:
				iter(remove_developers)
			except TypeError:
				remove_developers = [remove_developers]
			
			for dev in remove_developers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						remove_developers_ret.append((1,None))
						ok=1
						continue
				game.developers.remove(dev)
				remove_developers_ret.append((0,None))
			
			if(ok):
				errors.append(('remove_developers',ok,remove_developers_ret))
	
	if(boxart != None):
		if(isinstance(boxart,File) or isinstance(boxart,StringIO) or isinstance(boxart,file)):
			boxart = boxart.read()
		if(not game.save_images_from_raw(boxart)):
			errors.append(('boxart',1,None))
	
	game.game_global.save()
	game.save()
	
	if(len(errors)):
		return (1,game,errors)
	else:
		return (0,game,[])

@requirePerm("add")
@transaction.atomic
def add_studio(
		request,
		name,
		synopsis=None,
		synopsis_source_name=None,
		synopsis_source_link=None,
		alternative_names=None,
		add_alternative_names=None,
		remove_alternative_names=None,
		image=None):
	'''
		Return codes:
			0. Studio created successfully:
				Returns Studio object
			
			1. Studio created with some errors:
				Returns (Studio,(field,error_code,extra_return)+)
					'image':
						1. Image is not valid
							Returns None
					'synopsis_source_link':
						1. Not an valid URL
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
			
			4. Studio not created. Name empty:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	if(not name):
		return (4,None)
	
	if(synopsis == None):
		synopsis = ""
	
	studio = Studio.objects.create(
		name=name,
		synopsis=synopsis,
	)
	
	return edit_studio(
		request=request,
		studio=studio,
		name=None,
		synopsis=None,
		synopsis_source_name=synopsis_source_name,
		synopsis_source_link=synopsis_source_link,
		alternative_names=alternative_names,
		add_alternative_names=add_alternative_names,
		remove_alternative_names=remove_alternative_names,
		image=image,
	)

@requirePerm("add")
@transaction.atomic
def edit_studio(
		request,
		studio,
		name=None,
		synopsis=None,
		synopsis_source_name=None,
		synopsis_source_link=None,
		alternative_names=None,
		add_alternative_names=None,
		remove_alternative_names=None,
		image=None):
	'''
		Return codes:
			0. Studio edited successfully:
				Returns Studio object
			
			1. Studio edited with some errors:
				Returns (Studio,(field,error_code,extra_return)+)
					'name':
						1. Name is empty
							Returns None
					'image':
						1. Image is not valid
							Returns None
					'synopsis_source_link':
						1. Not an valid URL
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
			
			4. Studio not found:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	studio = get_studio(request,studio)
	
	if(studio[0]!=0):
		return (4,None)
	
	studio = studio[1]
	
	errors = []
	
	if(name != None):
		if(not name):
			errors.append(('name',1,None))
		else:
			studio.name = name
	
	if(synopsis != None):
		studio.synopsis = synopsis
	
	if(synopsis_source_name != None):
		studio.synopsis_source_name = synopsis_source_name
	
	if(synopsis_source_link != None):
		if(not synopsis_source_link.startswith(("http://","https://"))):
			synopsis_source_link = "http://" + synopsis_source_link
		synopsis_source_link = urlparse(synopsis_source_link)
		if(not ('.' in synopsis_source_link.netloc)):
			errors.append(('synopsis_source_link',1,None))
		else:
			studio.synopsis_source_link = synopsis_source_link.geturl()
	
	if(image != None):
		if(isinstance(image,File) or isinstance(image,StringIO) or isinstance(image,file)):
			image = image.read()
		if(not studio.save_images_from_raw(image)):
			errors.append(('image',1,None))
	
	if(alternative_names == None and (add_alternative_names!=None or remove_alternative_names!=None)):
		alternative_names = [i.name for i in list(studio.alternative_names.all())]
		if(add_alternative_names != None):
			for i in xrange(len(add_alternative_names)):
				if(not(add_alternative_names[i] in alternative_names)):
					alternative_names.append(add_alternative_names[i])
		if(remove_alternative_names != None):
			for i in xrange(len(remove_alternative_names)):
				if(remove_alternative_names[i] in alternative_names):
					alternative_names.remove(remove_alternative_names[i])
	
	if(alternative_names != None):
		alternative_names = [i.strip() for i in alternative_names]
		
		cur_alternative_names = list(studio.alternative_names.all())
		
		for alt_name in cur_alternative_names:
			if(not(alt_name.name in alternative_names)):
				alt_name.delete();
		
		cur_alternative_names = [i.name for i in cur_alternative_names]
		
		for alt_name in alternative_names:
			if(not(alt_name in cur_alternative_names)):
				studio.alternative_names.add(extensions.models.AlternativeName(name=alt_name));
	
	studio.save()
	
	if(len(errors)):
		return (1,studio,errors)
	else:
		return (0,studio,errors)

@requirePerm("add")
@transaction.atomic
def add_platform(
		request,
		name,
		synopsis=None,
		synopsis_source_name=None,
		synopsis_source_link=None,
		manufacturer=None,
		alternative_names=None,
		add_alternative_names=None,
		remove_alternative_names=None,
		image=None):
	'''
		Return codes:
			0. Platform created successfully:
				Returns Platform object
			
			1. Platform created with some errors:
				Returns (Platform,(field,error_code,extra_return)+)
					'manufacturer':
						1. Manufacturer not found
							Returns None
					'image':
						1. Image is not valid
							Returns None
					'synopsis_source_link':
						1. Not an valid URL
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
			
			4. Platform not created. Name empty:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	if(not name):
		return (4,None)
	
	platform = Platform.objects.create(
		name=name,
	)
	
	return(edit_platform(request,platform,None,
		synopsis = synopsis,
		synopsis_source_name = synopsis_source_name,
		synopsis_source_link = synopsis_source_link,
		manufacturer = manufacturer,
		alternative_names=alternative_names,
		add_alternative_names=add_alternative_names,
		remove_alternative_names=remove_alternative_names,
		image = image,
	))

@requirePerm("add")
@transaction.atomic
def edit_platform(
		request,
		platform,
		name=None,
		synopsis=None,
		synopsis_source_name=None,
		synopsis_source_link=None,
		manufacturer=None,
		alternative_names=None,
		add_alternative_names=None,
		remove_alternative_names=None,
		image=None):
	'''
		Return codes:
			0. Platform edited successfully:
				Returns Platform object
			
			1. Platform edited with some errors:
				Returns (Platform,(field,error_code,extra_return)+)
					'name':
						1. Name is empty
							Returns None
					'manufacturer':
						1. Manufacturer not found
							Returns None
					'image':
						1. Image is not valid
							Returns None
					'synopsis_source_link':
						1. Not an valid URL
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
			
			4. Platform not found:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	platform = get_platform(request,platform)
	
	if(platform[0]!=0):
		return (4,None)
	
	platform = platform[1]
	
	errors = []
	
	if(name != None):
		if(not name):
			errors.append(('name',1,None))
		else:
			platform.name = name
	
	if(synopsis != None):
		platform.synopsis = synopsis
	
	if(synopsis_source_name != None):
		platform.synopsis_source_name = synopsis_source_name
	
	if(synopsis_source_link != None):
		if(not synopsis_source_link.startswith(("http://","https://"))):
			synopsis_source_link = "http://" + synopsis_source_link
		synopsis_source_link = urlparse(synopsis_source_link)
		if(not ('.' in synopsis_source_link.netloc)):
			errors.append(('synopsis_source_link',1,None))
		else:
			platform.synopsis_source_link = synopsis_source_link.geturl()
	
	if(manufacturer != None):
		if(not isinstance(manufacturer,Studio)):
			manufacturer = get_studio(request,manufacturer)
			if(manufacturer[0]==1):
				errors.append(('manufacturer',1,None))
			else:
				platform.manufacturer = manufacturer[1]
	
	if(image != None):
		if(isinstance(image,File) or isinstance(image,StringIO) or isinstance(image,file)):
			image = image.read()
		if(not platform.save_images_from_raw(image)):
			errors.append(('image',1,None))
	
	if(alternative_names == None and (add_alternative_names!=None or remove_alternative_names!=None)):
		alternative_names = [i.name for i in list(platform.alternative_names.all())]
		if(add_alternative_names != None):
			for i in xrange(len(add_alternative_names)):
				if(not(add_alternative_names[i] in alternative_names)):
					alternative_names.append(add_alternative_names[i])
		if(remove_alternative_names != None):
			for i in xrange(len(remove_alternative_names)):
				if(remove_alternative_names[i] in alternative_names):
					alternative_names.remove(remove_alternative_names[i])
	
	if(alternative_names != None):
		alternative_names = [i.strip() for i in alternative_names]
		
		cur_alternative_names = list(platform.alternative_names.all())
		
		for alt_name in cur_alternative_names:
			if(not(alt_name.name in alternative_names)):
				alt_name.delete();
		
		cur_alternative_names = [i.name for i in cur_alternative_names]
		
		for alt_name in alternative_names:
			if(not(alt_name in cur_alternative_names)):
				platform.alternative_names.add(extensions.models.AlternativeName(name=alt_name));
	
	platform.save()
	
	if(len(errors)):
		return (1,platform,errors)
	else:
		return (0,platform,errors)

@requirePerm(None)
@transaction.atomic
def add_list_entry(request,game,status,score=None):
	'''
		Return Codes:
			0. List Entry edited successfully:
				Returns ListEntry object
			
			1. List Entry edited with some errors:
				Returns (ListEntry,(field,error_code,extra_return)+)
					'score':
						1. Score is not valid
							Returns None
			
			2. Game not found:
				Returns None
			
			3. User not logged in:
				Returns None
			
			4. User doesn't have an VGList account:
				Returns None
			
			5. Status not valid:
				Returns None
			
			6. ListEntry already exists:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (3,None)
	elif(user[0]==2):
		return (4,None)
	
	user = user[1]
	
	game = get_game(request,game)
	
	if(game[0]!=0):
		return (2,None)
	
	game = game[1]
	
	try:
		status = int(status)
	except ValueError:
		return (5,None)
	else:
		if(not status in LIST_STATUS):
			return (5,None)
	
	try:
		list_entry = ListEntry.objects.create(
			user = user,
			game = game,
			status = status,
		)
	except IntegrityError:
		return (6,None)
	
	return edit_list_entry(request,game,None,score)

@requirePerm(None)
@transaction.atomic
def edit_list_entry(request,game,status=None,score=None):
	'''
		Return Codes:
			0. List Entry edited successfully:
				Returns ListEntry object
			
			1. List Entry edited with some errors:
				Returns (ListEntry,(field,error_code,extra_return)+)
					'status':
						1. Status is not valid
							Returns None
					'score':
						1. Score is not valid
							Returns None
			
			2. List Entry not found:
				Returns None
	'''
	
	user = get_logged_vglistuser(request);
	
	if(user[0] != 0):
		return (2,None)
	
	user = user[1]
	
	list_entry = get_list_entry(request,user,game)
	
	if(list_entry[0] != 0):
		return (2,None)
	
	list_entry = list_entry[1]
	
	errors = []
	
	if(status != None):
		try:
			status = int(status)
		except ValueError:
			errors.append(('status',1,None))
		else:
			if(status in LIST_STATUS):
				list_entry.status = status
			else:
				errors.append(('status',1,None))
	
	if(score != None):
		if(score == ""):
			list_entry.score = None
		else:
			if(isinstance(score,float)):
				score = Decimal(floor(score*10))/10
			try:
				score = Decimal(score)
			except ValueError:
				errors.append(('score',1,None))
			else:
				if(score>100 or score<0):
					errors.append(('score',1,None))
				else:
					list_entry.score = score
	
	list_entry.save()
	
	if(len(errors)):
		return (1,list_entry,errors)
	else:
		return (0,list_entry,errors)

@requirePerm(None)
@transaction.atomic
def remove_list_entry(request,game):
	'''
		Return codes:
			0. List Entry removed successfully:
				Returns None
			
			1. List Entry Not found:
				Returns None
	'''
	
	user = get_logged_vglistuser(request);
	
	if(user[0] != 0):
		return (1,None)
	
	user = user[1]
	
	list_entry = get_list_entry(request,user,game)
	
	if(list_entry[0] != 0):
		return (1,None)
	
	list_entry[1].delete()
	
	return (0,None)

@requirePerm(None)
@transaction.atomic
def get_studios_by_prefix(request,prefix):
	'''
		Return Codes:
			0. Studio found:
				Return list<Studio>
				
			1. Studio not found:
				Return empty list
				
			2. Prefix is too small:
				Return empty list
	'''
	if(len(prefix)<1):
		return (2,[])
	
	studios = Studio.objects.filter(name__istartswith=prefix)
	
	return (int(not bool(studios.count())),studios)

@requirePerm(None)
@transaction.atomic
def get_platforms_by_prefix(request,prefix):
	'''
		Return Codes:
			0. Platform found:
				Return list<Platform>
				
			1. Platform not found:
				Return empty list
				
			2. Prefix is too small:
				Return empty list
	'''
	if(len(prefix)<1):
		return (2,[])
	
	platforms = Platform.objects.filter(name__istartswith=prefix)
	
	return (int(not bool(platforms.count())),platforms)

@requirePerm(None)
@transaction.atomic
def search_game(request,name_query=None,min_release_date=None,max_release_date=None,publisher=None,developer=None,platform=None, offset=0, results_per_page=0):
	'''
		Return Codes:
			0. Game found:
				Return QuerySet<GameLocal>, has_next_page<bool>, has_prev_page<bool>
			
			1. Game not found:
				Return empty list, False, False
			
			2. No filters informed:
				Return empty list, False, False
	'''
	
	offset = max(0,int(offset))
	results_per_page = int(results_per_page)
	
	if(results_per_page >= ALLOWED_RESULTS_PER_PAGE[-1]):
		results_per_page = ALLOWED_RESULTS_PER_PAGE[-1]
	else:
		for i in ALLOWED_RESULTS_PER_PAGE:
			if(results_per_page <= i):
				results_per_page = i
				break
	
	offset = results_per_page*(offset/results_per_page)
	
	filtered = False
	
	query = GameGlobal.objects.all()
	
	if(name_query != None):
		name_query = unidecode(name_query)
		name_query = filter(lambda x:len(x)>1,name_query.split())
		if(len(name_query)):
			filtered = True
			t_query = query.filter(search_ready_name__icontains=name_query[0]) | query.filter(gamelocal__search_ready_name__icontains=name_query[0])
			for i in name_query[1:]:
				t_query = t_query & (query.filter(search_ready_name__icontains=i) | query.filter(gamelocal__search_ready_name__icontains=i))
			query = t_query
	
	if(min_release_date != None):
		if(not isinstance(min_release_date,date)):
			try:
				min_release_date = datetime.strptime(min_release_date,'%Y-%m-%d')
			except ValueError:
				min_release_date = None
		if(min_release_date):
			filtered = True
			query = query.filter(gamelocal__release_date__gte=min_release_date)
	
	if(max_release_date != None):
		if(not isinstance(max_release_date,date)):
			try:
				max_release_date = datetime.strptime(max_release_date,'%Y-%m-%d')
			except ValueError:
				max_release_date = None
		if(max_release_date):
			filtered = True
			query = query.filter(gamelocal__release_date__lte=max_release_date).exclude(gamelocal__release_date__in=["%04d-%02d-00" % (max_release_date.year,max_release_date.month),"%04d-00-00" % (max_release_date.year)])
	
	if(publisher != None):
		publisher = get_studio(request,publisher)
		
		if(publisher[0] !=1):
			query = query.filter(gamelocal__publishers = publisher[1])
			filtered = True
	
	if(developer != None):
		developer = get_studio(request,developer)
		
		if(developer[0] !=1):
			query = query.filter(gamelocal__developers = developer[1])
			filtered = True
	
	if(platform != None):
		platform = get_platform(request,platform)
		
		if(platform[0] !=1):
			query = query.filter(gamelocal__console = platform[1])
			filtered = True
	
	if(not filtered):
		return (2,[],False,False)
	
	query = query.distinct()
	
	query = query[offset:offset+results_per_page+1]
	
	if(bool(query)):
		has_next_page = (query.count() > results_per_page)
		has_prev_page = (offset >= results_per_page)
		query = query[:results_per_page]
		return (0,query,has_next_page,has_prev_page)
	else:
		return (1,[],False,False)

@requirePerm(None)
@transaction.atomic
def search_platform(request,name_query=None,manufacturer=None, offset=0, results_per_page=0):
	'''
		Return Codes:
			0. Platform found:
				Return QuerySet<Platform>, has_next_page<bool>, has_prev_page<bool>
			
			1. Platform not found:
				Return empty list, False, False
			
			2. No filters informed:
				Return empty list, False, False
	'''
	
	offset = max(0,int(offset))
	results_per_page = int(results_per_page)
	
	if(results_per_page >= ALLOWED_RESULTS_PER_PAGE[-1]):
		results_per_page = ALLOWED_RESULTS_PER_PAGE[-1]
	else:
		for i in ALLOWED_RESULTS_PER_PAGE:
			if(results_per_page <= i):
				results_per_page = i
				break
	
	offset = results_per_page*(offset/results_per_page)
	
	filtered = False
	
	query = Platform.objects.all()
	
	if(name_query != None):
		name_query = unidecode(name_query)
		name_query = filter(lambda x:len(x)>1,name_query.split())
		if(len(name_query)):
			filtered = True
			t_query = query.filter(search_ready_name__icontains=name_query[0])
			for i in name_query[1:]:
				t_query = t_query & query.filter(search_ready_name__icontains=i)
			query = t_query
	
	if(manufacturer != None):
		query = query.filter(manufacturer = manufacturer)
		filtered = True
	
	if(not filtered):
		return (2,[],False,False)
	
	query = query[offset:offset+results_per_page+1]
	
	if(query.exists()):
		has_next_page = (query.count() > results_per_page)
		has_prev_page = (offset >= results_per_page)
		query = query[:results_per_page]
		return (0,query,has_next_page,has_prev_page)
	else:
		return (1,[],False,False)

@requirePerm(None)
@transaction.atomic
def search_studio(request,name_query=None, offset=0, results_per_page=0):
	'''
		Return Codes:
			0. Studio found:
				Return QuerySet<Studio>, has_next_page<bool>, has_prev_page<bool>
			
			1. Studio not found:
				Return empty list, False, False
			
			2. No filters informed:
				Return empty list, False, False
	'''
	
	offset = max(0,int(offset))
	results_per_page = int(results_per_page)
	
	if(results_per_page >= ALLOWED_RESULTS_PER_PAGE[-1]):
		results_per_page = ALLOWED_RESULTS_PER_PAGE[-1]
	else:
		for i in ALLOWED_RESULTS_PER_PAGE:
			if(results_per_page <= i):
				results_per_page = i
				break
	
	offset = results_per_page*(offset/results_per_page)
	
	filtered = False
	
	query = Studio.objects.all()
	
	if(name_query != None):
		name_query = unidecode(name_query)
		name_query = filter(lambda x:len(x)>1,name_query.split())
		if(len(name_query)):
			filtered = True
			t_query = query.filter(search_ready_name__icontains=name_query[0])
			for i in name_query[1:]:
				t_query = t_query & query.filter(search_ready_name__icontains=i)
			query = t_query
	
	if(not filtered):
		return (2,[],False,False)
	
	query = query[offset:offset+results_per_page+1]
	
	if(query.exists()):
		has_next_page = (query.count() > results_per_page)
		has_prev_page = (offset >= results_per_page)
		query = query[:results_per_page]
		return (0,query,has_next_page,has_prev_page)
	else:
		return (1,[],False,False)

@requirePerm(None)
@transaction.atomic
def get_global_statistics(request, statistics):
	'''
		Return Codes:
			0. Statistics found:
				Returns Dict<statistic_name,value|None>
	'''
	
	ret = {}
	
	for i in statistics:
		if(i == 'vglistuser_count'):
			ret[i] = VGListUser.objects.filter(user__is_active=True).count()
		elif(i == 'gameglobal_count'):
			ret[i] = GameGlobal.objects.count()
		elif(i == 'gamelocal_count'):
			ret[i] = GameLocal.objects.count()
		elif(i == 'studio_count'):
			ret[i] = Studio.objects.count()
		elif(i == 'platform_count'):
			ret[i] = Platform.objects.count()
		elif(i == 'listentry_count'):
			ret[i] = ListEntry.objects.count()
		elif(i == 'vglistcomment_count'):
			ret[i] = VGListComment.objects.count()
		elif(i == 'vglistuser_online_count'):
			ret[i] = VGListUser.objects.filter(user__last_online__gte=(timezone.now() - timedelta(minutes=5))).count()
		else:
			ret[i] = None
	
	return (0,ret)

@requirePerm(None)
@transaction.atomic
def add_friend(request,user):
	'''
		Return Codes:
			0. Friend request sent:
				Returns None
			
			1. User not found:
				Returns None
			
			2. Friend request already sent:
				Returns None
			
			3. User not logged in:
				Returns None
			
			4. Users already are friends. Please use facebook to make your relationship more serious:
				Returns None
			
			5. One cant add itself as friend:
				Returns None
	'''
	
	me = get_logged_vglistuser(request);
	
	if(me[0]!=0):
		return (3,None)
	
	me = me[1]
	
	user = get_vglistuser(request,user)
	
	if(user[0]!=0):
		return (1,None)
	
	user = user[1]
	
	if(user.pk == me.pk):
		return (5,None)
	
	try:
		friend = Friendship.objects.get(vglistuser_active__in=[me,user],vglistuser_passive__in=[me,user])
		if(friend.status == Friendship.STATUS_CHOICES.reverse_get("Pending")):
			if(friend.vglistuser_source.pk == me.pk):
				return (2,None)
			else:
				friend.status = Friendship.STATUS_CHOICES.reverse_get("Confirmed")
				friend.save()
				return (0,None)
		else:
			return (4,None)
	except Friendship.DoesNotExist: # O=
		Friendship.objects.create(vglistuser_source=me, vglistuser_active=me, vglistuser_passive=user)
		return (0,None)

@requirePerm(None)
@transaction.atomic
def remove_friend(request,username):
	'''
		Return Codes:
			0. Friend removed:
				Return None
			
			1. Friendship does not exist:
				Return None
			
			2. User not logged in:
				Return None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0] != 0):
		return (2,None)
	
	user = user[1]
	
	try:
		friend = Friendship.objects.get(vglistuser_active__in=[user,username],vglistuser_passive__in=[user,username])
		friend.delete()
		return (0,None)
	except:
		return (1,None)

@requirePerm(None)
@transaction.atomic
def accept_friend_request(request,username):
	'''
		Return Codes:
			0. Request accepted:
				Return None
			
			1. Friendship does not exist:
				Return None
			
			2. User not logged in:
				Return None
			
			3. Friendship already confirmed:
				Return None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0] != 0):
		return (2,None)
	
	user = user[1]
	
	try:
		friend = Friendship.objects.get(vglistuser_active__in=[user,username],vglistuser_passive__in=[user,username])
		
		if(friend.status == Friendship.STATUS_CHOICES.reverse_get("Pending")):
			friend.status = Friendship.STATUS_CHOICES.reverse_get("Confirmed")
			friend.save()
			return (0,None)
		else:
			return (3,None)
	except:
		return (1,None)

@requirePerm(None)
@transaction.atomic
def reject_friend_request(request,username):
	'''
		Return Codes:
			0. Request rejected:
				Return None
			
			1. Friendship does not exist:
				Return None
			
			2. User not logged in:
				Return None
			
			3. Friendship already confirmed:
				Return None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0] != 0):
		return (2,None)
	
	user = user[1]
	
	try:
		friend = Friendship.objects.get(vglistuser_active__in=[user,username],vglistuser_passive__in=[user,username])
		
		if(friend.status == Friendship.STATUS_CHOICES.reverse_get("Pending")):
			friend.delete()
			return (0,None)
		else:
			return (3,None)
	except:
		return (1,None)

class VGListUserController(Controller):
	STATUS_USER_FOUND = 1;
	STATUS_USER_NOT_FOUND = 2;
	
	_model = None
	
	def _create(self,vglistuser):
		if(not isinstance(vglistuser,VGListUser)):
			if(vglistuser == "me"):
				if(not self.request.user.is_authenticated()):
					return VGListUserController.STATUS_USER_NOT_FOUND
				
				try:
					self._model = self.request.user.vglistuser
				except VGListUser.DoesNotExist:
					return VGListUserController.STATUS_USER_NOT_FOUND
				
				return VGListUserController.STATUS_USER_FOUND
			try:
				vglistuser = VGListUser.objects.get(pk=vglistuser.lower())
			except VGListUser.DoesNotExist:
				return self.STATUS_USER_NOT_FOUND
		
		self._model = vglistuser
		
		return self.STATUS_USER_FOUND
	
	@assertStatus(STATUS_USER_FOUND)
	def friends(self):
		return [VGListUserController(self.request,i) for i in sorted(
			[i.vglistuser_passive for i in self._model.friends_passive.filter(status = Friendship.STATUS_CHOICES.reverse_get("Confirmed"))] +
			[i.vglistuser_active for i in self._model.friends_active.filter(status = Friendship.STATUS_CHOICES.reverse_get("Confirmed"))]
		,key=lambda user: user.user.username)]
	
	@assertStatus(STATUS_USER_FOUND)
	def friend_count(self):
		return self._model.friends_passive.filter(status = Friendship.STATUS_CHOICES.reverse_get("Confirmed")).count() + self._model.friends_active.filter(status = Friendship.STATUS_CHOICES.reverse_get("Confirmed")).count()
	
	@assertStatus(STATUS_USER_FOUND)
	def username(self):
		return self._model.user.username
	
	@assertStatus(STATUS_USER_FOUND)
	def bio(self):
		return self._model.bio
	
	@assertStatus(STATUS_USER_FOUND)
	def can_receive_mail(self):
		return self._model.can_receive_mail
	
	@assertStatus(STATUS_USER_FOUND)
	def comments(self):
		return self._model.comments.all()
	
	@assertStatus(STATUS_USER_FOUND)
	def is_admin(self):
		return self._model.user.is_admin
	
	@assertStatus(STATUS_USER_FOUND)
	def join_time(self):
		return self._model.user.join_time
	
	@assertStatus(STATUS_USER_FOUND)
	def last_online(self):
		return self._model.user.last_online
	
	@assertStatus(STATUS_USER_FOUND)
	def gender(self):
		return self._model.user.get_gender_display()
	
	@assertStatus(STATUS_USER_FOUND)
	def gender_id(self):
		return self._model.user.gender
	
	@assertStatus(STATUS_USER_FOUND)
	def avatar(self):
		try:
			return self._model.user.avatar.url
		except ValueError:
			return None
	
	@assertStatus(STATUS_USER_FOUND)
	def thumbnail(self):
		try:
			return self._model.user.thumbnail().url
		except ValueError:
			return None
	
	@assertStatus(STATUS_USER_FOUND)
	def banner(self):
		try:
			return self._model.banner.url
		except ValueError:
			return None
	
	@assertStatus(STATUS_USER_FOUND)
	def list_count(self):
		return self._model.listentry_set.count()
	
	@assertStatus(STATUS_USER_FOUND)
	def list(self):
		return self._model.listentry_set.all()
	
	@assertStatus(STATUS_USER_FOUND)
	def is_friend(self):
		user = get_logged_vglistuser(self.request)
		
		if(user[0] == 0):
			user = user[1]
		else:
			return False
		
		return Friendship.objects.filter(vglistuser_passive__in=[user.user.username,self.username()],vglistuser_active__in=[user.user.username,self.username()],status=Friendship.STATUS_CHOICES.reverse_get("Confirmed")).exists()
	
	@assertStatus(STATUS_USER_FOUND)
	def is_pending_friend(self):
		user = get_logged_vglistuser(self.request)
		
		if(user[0] == 0):
			user = user[1]
		else:
			return False
		
		return Friendship.objects.filter(vglistuser_passive__in=[user.user.username,self.username()],vglistuser_active__in=[user.user.username,self.username()],status=Friendship.STATUS_CHOICES.reverse_get("Pending")).exists()
	
	@assertStatus(STATUS_USER_FOUND)
	def is_me(self):
		user = get_logged_vglistuser(self.request)
		
		if(user[0] == 0):
			user = user[1]
		else:
			return False
		
		return user.user.username == self.username()

class LoggedVGListUserController(VGListUserController):
	def _create(self):
		if(not self.request.user.is_authenticated()):
			return VGListUserController.STATUS_USER_NOT_FOUND
		
		try:
			self._model = self.request.user.vglistuser
		except VGListUser.DoesNotExist:
			return VGListUserController.STATUS_USER_NOT_FOUND
		
		return VGListUserController.STATUS_USER_FOUND
	
	@assertStatus(VGListUserController.STATUS_USER_FOUND)
	def pending_friend_requests_sent(self):
		return [VGListUserController(self.request,i) for i in sorted(
			[i.vglistuser_passive for i in self._model.friends_passive.filter(status = Friendship.STATUS_CHOICES.reverse_get("Pending")).filter(vglistuser_source = self._model)] +
			[i.vglistuser_active for i in self._model.friends_active.filter(status = Friendship.STATUS_CHOICES.reverse_get("Pending")).filter(vglistuser_source = self._model)]
		,key=lambda user: user.user.username)]
	
	@assertStatus(VGListUserController.STATUS_USER_FOUND)
	def pending_friend_requests_received(self):
		return [VGListUserController(self.request,i) for i in sorted(
			[i.vglistuser_passive for i in self._model.friends_passive.filter(status = Friendship.STATUS_CHOICES.reverse_get("Pending")).exclude(vglistuser_source = self._model)] +
			[i.vglistuser_active for i in self._model.friends_active.filter(status = Friendship.STATUS_CHOICES.reverse_get("Pending")).exclude(vglistuser_source = self._model)]
		,key=lambda user: user.user.username)]

class VGListUserSearchController(SearchController):
	Controller = VGListUserController
	
	def _create(self, username=None, gender=None):
		query = VGListUser.objects.all()
		
		if(username != None):
			username = unidecode(username)
			username = filter(lambda x:len(x)>1,username.split())
			
			if(len(username)):
				t_query = query.filter(user__username__icontains=username[0])
				
				for i in username[1:]:
					t_query = t_query & query.filter(user__username__icontains=i)
				
				query = t_query
		
		if(gender != None):
			query = query.filter(user__gender=gender)
		
		self.set_query(query)
	
class VGListStaffController(Controller):
	def _create(self,model):
		self._model = model
	
	def user(self):
		return VGListUserController(self.request, self._model.vglistuser)
	
	def role(self):
		return self._model.role

class VGListStatistics(Controller):
	def staff(self):
		return [VGListStaffController(self.request,i) for i in VGListStaff.objects.all().order_by('role','vglistuser')]

class NewsController(Controller):
	STATUS_NEWS_FOUND = 1
	STATUS_NEWS_NOT_FOUND = 2
	
	_model = None
	
	def _create(self,news):
		if(news == None):
			return NewsController.STATUS_NEWS_NOT_FOUND
		
		if(not isinstance(news,News)):
			try:
				news = News.objects.get(pk=news)
			except News.DoesNotExist:
				return NewsController.STATUS_NEWS_NOT_FOUND
		
		if(news.is_public() or EditNewsController.can(self.request)):
			self._model = news
			return NewsController.STATUS_NEWS_FOUND
		else:
			return NewsController.STATUS_NEWS_NOT_FOUND
	
	@assertStatus(STATUS_NEWS_FOUND)
	def id(self):
		return self._model.id
	
	@assertStatus(STATUS_NEWS_FOUND)
	def poster(self):
		return VGListUserController(self.request,self._model.poster)
	
	@assertStatus(STATUS_NEWS_FOUND)
	def title(self):
		return self._model.title if self._model.title else ""
	
	@assertStatus(STATUS_NEWS_FOUND)
	def time(self):
		return self._model.time
	
	@assertStatus(STATUS_NEWS_FOUND)
	def body(self):
		return self._model.body if self._model.body else ""
	
	@assertStatus(STATUS_NEWS_FOUND)
	def split_body(self):
		return self._model.body.strip().split() if self.body().trim() else []
	
	@assertStatus(STATUS_NEWS_FOUND)
	def comments(self):
		return self._model.comments.all()
	
	@assertStatus(STATUS_NEWS_FOUND)
	def is_published(self):
		return self._model.is_public()

class NewsSearchController(SearchController):
	Controller = NewsController
	
	def _create(self, published=True, own=False, order='time', decrescent=True):
		if(published != True and not EditNewsController.can(self.request)):
			published = True
		
		query = News.objects.all()
		
		if(published != None):
			if(published):
				query = query.filter_published()
			else:
				query = query.exclude_published()
		
		if(own):
			user = LoggedVGListUserController(self.request)
			if(user.status == user.STATUS_USER_FOUND):
				query = query.filter(poster=user.username())
		
		if(order == 'time'):
			order = "time"
			if(decrescent):
				order = "-" + order
			query = query.order_by(order)
		
		self.set_query(query)

class InteractNewsController(NewsController):
	logged_vglistuser_controller = None;
	
	def _create(self,news):
		self.logged_vglistuser_controller = LoggedVGListUserController(self.request)
		
		if(self.logged_vglistuser_controller.status == LoggedVGListUserController.STATUS_USER_FOUND):
			return NewsController._create(self,news)
		else:
			return NewsController.STATUS_NEWS_NOT_FOUND
	
	@assertStatus(NewsController.STATUS_NEWS_FOUND)
	def comment(self,comment):
		VGListComment.objects.create(
			comment = people.models.Comment.objects.create(
				text = comment,
			),
			commenter = self.logged_vglistuser_controller._model,
			content_object = self._model,
		)

@requirePerm('post_news')
class EditNewsController(NewsController):
	def _create(self,news):
		if(news == None):
			return NewsController.STATUS_NEWS_NOT_FOUND
		
		if(not isinstance(news,News)):
			try:
				news = News.objects.get(pk=news)
			except News.DoesNotExist:
				return NewsController.STATUS_NEWS_NOT_FOUND
		
		if(not news.is_public() and news.poster == LoggedVGListUserController(self.request)._model):
			self._model = news
			return NewsController.STATUS_NEWS_FOUND
		else:
			return NewsController.STATUS_NEWS_NOT_FOUND
	
	@assertStatus(NewsController.STATUS_NEWS_FOUND)
	def update_title(self,title):
		self._model.title = title if title else ""
		self._model.save()
	
	@assertStatus(NewsController.STATUS_NEWS_FOUND)
	def update_body(self,body):
		self._model.body = body if body else ""
		self._model.save()
	
	@assertStatus(NewsController.STATUS_NEWS_FOUND)
	def can_be_published(self):
		return bool(self._model.title) and bool(self._model.body)
	
	@assertStatus(NewsController.STATUS_NEWS_FOUND)
	def publish_now(self):
		if(not self.can_be_published()):
			return False
		
		self._model.time = timezone.now()
		self._model.save()
		self.status = NewsController.STATUS_NEWS_NOT_FOUND if self._model.is_public() else NewsController.STATUS_NEWS_FOUND
		
		return True
	
	@assertStatus(NewsController.STATUS_NEWS_FOUND)
	def schedule_publish(self,time):
		if(not self.can_be_published()):
			return False
		
		if(time != None and (not isinstance(time,datetime))):
			try:
				time = datetime.strptime(time,"%Y-%m-%d %H:%M:%S")
			except:
				return False
		
		self._model.time = time
		self._model.save()
		self.status = NewsController.STATUS_NEWS_NOT_FOUND if self._model.is_public() else NewsController.STATUS_NEWS_FOUND
		
		return True
	
	@assertStatus(NewsController.STATUS_NEWS_FOUND)
	def throw_away(self):
		self._model.delete()
		self.status = NewsController.STATUS_NEWS_NOT_FOUND

@requirePerm('post_news')
class CreateNewsController(EditNewsController):
	def _create(self):
		return EditNewsController._create(
			self,
			News.objects.create(
				poster = LoggedVGListUserController(self.request)._model
			)
		)

class StudioController(Controller):
	STATUS_STUDIO_FOUND = 1
	STATUS_STUDIO_NOT_FOUND = 2
	
	_model = None
	
	def _create(self,studio):
		if(studio == None):
			return StudioController.STATUS_STUDIO_NOT_FOUND
		
		if(not isinstance(studio,Studio)):
			try:
				studio = Studio.objects.get(pk=studio)
			except Studio.DoesNotExist:
				return StudioController.STATUS_STUDIO_NOT_FOUND
		
		self._model = studio
		return StudioController.STATUS_STUDIO_FOUND
	
	@assertStatus(STATUS_STUDIO_FOUND)
	def id(self):
		return self._model.id
	
	@assertStatus(STATUS_STUDIO_FOUND)
	def name(self):
		return self._model.name
	
	@assertStatus(STATUS_STUDIO_FOUND)
	def short_name(self):
		return self._model.short_name()
	
	@assertStatus(STATUS_STUDIO_FOUND)
	def synopsis(self):
		return {
				"text": self._model.synopsis,
				"name": self._model.synopsis_source_name,
				"link": self._model.synopsis_source_link,
			}
	
	@assertStatus(STATUS_STUDIO_FOUND)
	def image(self):
		ret = {
				"original": None,
				"regular": None,
				"thumbnail": None,
			}
		
		try:
			ret["original"] = self._model.image.url
		except ValueError:
			pass
		
		try:
			ret["regular"] = self._model.regular().url
		except ValueError:
			pass
		
		try:
			ret["thumbnail"] = self._model.thumbnail().url
		except ValueError:
			pass
		
		return ret;
	
	@assertStatus(STATUS_STUDIO_FOUND)
	def comments(self):
		return self._model.comments.all()

class StudioSearchController(SearchController):
	Controller = StudioController
	
	def _create(self, name=None):
		query = Studio.objects.all()
		
		if(name != None):
			name = unidecode(name)
			name = filter(lambda x:len(x)>1,name.split())
			
			if(len(name)):
				t_query = query.filter(search_ready_name__icontains=name[0])
				
				for i in name[1:]:
					t_query = t_query & query.filter(search_ready_name__icontains=i)
				
				query = t_query
		
		self.set_query(query)

class PlatformController(Controller):
	STATUS_PLATFORM_FOUND = 1
	STATUS_PLATFORM_NOT_FOUND = 2
	
	_model = None
	
	def _create(self,platform):
		if(platform == None):
			return PlatformController.STATUS_PLATFORM_NOT_FOUND
		
		if(not isinstance(platform,Platform)):
			try:
				platform = Platform.objects.get(pk=platform)
			except Platform.DoesNotExist:
				return PlatformController.STATUS_PLATFORM_NOT_FOUND
		
		self._model = platform
		return PlatformController.STATUS_PLATFORM_FOUND
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def id(self):
		return self._model.id
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def name(self):
		return self._model.name
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def short_name(self):
		return self._model.short_name()
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def synopsis(self):
		return {
				"text": self._model.synopsis,
				"name": self._model.synopsis_source_name,
				"link": self._model.synopsis_source_link,
			}
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def image(self):
		ret = {
				"original": None,
				"regular": None,
				"thumbnail": None,
			}
		
		try:
			ret["original"] = self._model.image.url
		except ValueError:
			pass
		
		try:
			ret["regular"] = self._model.regular().url
		except ValueError:
			pass
		
		try:
			ret["thumbnail"] = self._model.thumbnail().url
		except ValueError:
			pass
		
		return ret;
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def comments(self):
		return self._model.comments.all()
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def alternative_names(self):
		return [i.name for i in self._model.alternative_names.all()]
	
	@assertStatus(STATUS_PLATFORM_FOUND)
	def manufacturer(self):
		ret = StudioController(self.request,self._model.manufacturer)
		
		if(ret.status == ret.STATUS_STUDIO_FOUND):
			return ret
		else:
			return None

class PlatformSearchController(SearchController):
	Controller = PlatformController
	
	def _create(self, name=None, manufacturer=None):
		query = Platform.objects.all()
		
		if(name != None):
			name = unidecode(name)
			name = filter(lambda x:len(x)>1,name.split())
			
			if(len(name)):
				t_query = query.filter(search_ready_name__icontains=name[0])
				
				for i in name[1:]:
					t_query = t_query & query.filter(search_ready_name__icontains=i)
				
				query = t_query
		
		if(manufacturer != None):
			if(isinstance(manufacturer,StudioController)):
				manufacturer = manufacturer._model
			
			query = query.filter(manufacturer = manufacturer)
		
		self.set_query(query)

class GameGlobalController(Controller):
	STATUS_GAME_GLOBAL_FOUND = 1
	STATUS_GAME_GLOBAL_NOT_FOUND = 2
	
	_model = None
	
	def _create(self,game_global):
		if(game_global == None):
			return GameGlobalController.STATUS_GAME_GLOBAL_NOT_FOUND
		
		if(not isinstance(game_global,GameGlobal)):
			try:
				game_global = GameGlobal.objects.get(pk=game_global)
			except GameGlobal.DoesNotExist:
				return GameGlobalController.STATUS_GAME_GLOBAL_NOT_FOUND
		
		self._model = game_global
		return GameGlobalController.STATUS_GAME_GLOBAL_FOUND
	
	@assertStatus(STATUS_GAME_GLOBAL_FOUND)
	def id(self):
		return self._model.id
	
	@assertStatus(STATUS_GAME_GLOBAL_FOUND)
	def name(self):
		return self._model.name
	
	@assertStatus(STATUS_GAME_GLOBAL_FOUND)
	def synopsis(self):
		return {
			'body': self._model.synopsis,
			'source_name': self._model.synopsis_source_name,
			'source_link': self._model.synopsis_source_link,
		}
	
	@assertStatus(STATUS_GAME_GLOBAL_FOUND)
	def score(self):
		return self._model.score()
	
	@assertStatus(STATUS_GAME_GLOBAL_FOUND)
	def game_locals(self):
		return [GameLocalController(self.request,i) for i in self._model.gamelocal_set.all()]

class GameLocalController(Controller):
	STATUS_GAME_LOCAL_FOUND = 1
	STATUS_GAME_LOCAL_NOT_FOUND = 2
	
	_model = None
	
	def _create(self,game_local,game_global=None):
		if(game_local == None):
			return GameLocalController.STATUS_GAME_LOCAL_NOT_FOUND
		
		if(not isinstance(game_local,GameLocal)):
			try:
				if(game_global == None):
					return GameLocalController.STATUS_GAME_LOCAL_NOT_FOUND
				if(isinstance(game_global,GameGlobal)):
					game_global = game_global.id
				game_local = GameLocal.objects.get(local_id=game_local, game_global=game_global)
			except GameLocal.DoesNotExist:
				return GameLocalController.STATUS_GAME_LOCAL_NOT_FOUND
		
		self._model = game_local
		return GameLocalController.STATUS_GAME_LOCAL_FOUND
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def id(self):
		return {
			'local': self._model.local_id,
			'global': self._model.game_global_id,
		}
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def game_global(self):
		return GameGlobalController(self.request,self._model.game_global)
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def name(self):
		return self._model.name
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def release_date(self):
		return self._model.release_date
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def publishers(self):
		return [StudioController(self.request,i) for i in self._model.publishers.all()]
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def developers(self):
		return [StudioController(self.request,i) for i in self._model.developers.all()]
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def boxart(self):
		ret = {
				"original": None,
				"high_definition": None,
				"regular": None,
				"thumbnail": None,
			}
		
		try:
			ret["original"] = self._model.boxart.url
		except ValueError:
			pass
		
		try:
			ret["high_definition"] = self._model.high_definition().url
		except ValueError:
			pass
		
		try:
			ret["regular"] = self._model.regular().url
		except ValueError:
			pass
		
		try:
			ret["thumbnail"] = self._model.thumbnail().url
		except ValueError:
			pass
		
		return ret;
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def console(self):
		ret = PlatformController(self.request,self._model.console)
		
		if(ret.status == ret.STATUS_PLATFORM_FOUND):
			return ret
		else:
			return None
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def comments(self):
		return self._model.comments.all()
	
	@assertStatus(STATUS_GAME_LOCAL_FOUND)
	def score(self):
		return self._model.score()

class GameResultController(GameLocalController):
	def _create(self,game_global):
		if(isinstance(game_global,GameGlobal)):
			return super(GameResultController,self)._create(game_global.gamelocal_set.all().order_by('local_id')[0])
		else:
			super(GameResultController,self)._create(game_global)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	def name(self):
		return self.game_global().name();

class GameSearchController(SearchController):
	Controller = GameResultController
	
	def _create(self, name=None, min_release_date=None, max_release_date=None, publisher=None, developer=None, platform=None):
		query = GameGlobal.objects.all()
		
		if(name != None):
			name = unidecode(name)
			name = filter(lambda x:len(x)>1,name.split())
			if(len(name)):
				t_query = query.filter(search_ready_name__icontains=name[0]) | query.filter(gamelocal__search_ready_name__icontains=name[0])
				for i in name[1:]:
					t_query = t_query & (query.filter(search_ready_name__icontains=i) | query.filter(gamelocal__search_ready_name__icontains=i))
				query = t_query
		
		if(min_release_date != None):
			if(not isinstance(min_release_date,date)):
				try:
					min_release_date = datetime.strptime(min_release_date,'%Y-%m-%d')
				except ValueError:
					min_release_date = None
			if(min_release_date):
				query = query.filter(gamelocal__release_date__gte=min_release_date)
		
		if(max_release_date != None):
			if(not isinstance(max_release_date,date)):
				try:
					max_release_date = datetime.strptime(max_release_date,'%Y-%m-%d')
				except ValueError:
					max_release_date = None
			if(max_release_date):
				query = query.filter(gamelocal__release_date__lte=max_release_date).exclude(gamelocal__release_date__in=["%04d-%02d-00" % (max_release_date.year,max_release_date.month),"%04d-00-00" % (max_release_date.year)])
		
		if(publisher != None):
			query = query.filter(gamelocal__publishers = publisher)
		
		if(developer != None):
			query = query.filter(gamelocal__developers = developer)
		
		if(platform != None):
			query = query.filter(gamelocal__console = platform)
		
		query = query.distinct()
		
		self.set_query(query)

@requirePerm("add")
class EditGameLocalController(GameLocalController):
	STATUS_GAME_EDIT_COMMITED = 4
	
	def _create(self,*args,**kwargs):
		temp = super(EditGameLocalController,self)._create(*args,**kwargs)
		
		if(temp == GameLocalController.STATUS_GAME_LOCAL_FOUND):
			self._model_original = self._model
			self._model = VGListEditState.objects.prepare_for_edit(self._model)
		
		return temp;
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_name(self,name):
		self._model.name = name
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_name(self,name):
		self._model.name = name
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_release_date(self,release_date):
		if(not isinstance(release_date,InaccurateDate)):
			if(not isinstance(release_date,date)):
				release_date = release_date
			elif(not release_date or release_date == "0000-00-00"):
				release_date = None
			else:
				release_date = InaccurateDateField().get_value(release_date)
		
		self._model.release_date = release_date
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_release_date(self,release_date):
		if(not isinstance(release_date,InaccurateDate)):
			if(not isinstance(release_date,date)):
				release_date = release_date
			elif(not release_date or release_date == "0000-00-00"):
				release_date = None
			else:
				release_date = InaccurateDateField().get_value(release_date)
		
		self._model.release_date = release_date
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_console(self,console):
		if(not isinstance(console,Platform)):
			if(isinstance(console,PlatformController)):
				console = console._model
			else:
				console = PlatformController(self.request,console)
				
				if(console.status == console.STATUS_PLATFORM_FOUND):
					console = console._model
				else:
					return
		
		self._model.console = console
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_developers(self,developers):
		try:
			iter(developers)
		except TypeError:
			developers = [developers]
		
		self._model.developers.clear()
		
		for devIter in developers:
			dev = devIter
			
			if(isinstance(dev,StudioController)):
					dev = dev._model
				
			self._model.developers.add(dev)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_developers_add(self,developers):
		try:
			iter(developers)
		except TypeError:
			developers = [developers]
		
		for devIter in developers:
			dev = devIter
			
			if(isinstance(dev,StudioController)):
					dev = dev._model
			
			self._model.developers.add(dev)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_developers_remove(self,developers):
		try:
			iter(developers)
		except TypeError:
			developers = [developers]
		
		for devIter in developers:
			dev = devIter
			
			if(isinstance(dev,StudioController)):
					dev = dev._model
			
			self._model.developers.remove(dev)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_publishers(self,publishers):
		try:
			iter(publishers)
		except TypeError:
			publishers = [publishers]
		
		self._model.publishers.clear()
		
		for pubIter in publishers:
			pub = pubIter
			
			if(isinstance(pub,StudioController)):
					pub = pub._model
				
			self._model.publishers.add(pub)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_publishers_add(self,publishers):
		try:
			iter(publishers)
		except TypeError:
			publishers = [publishers]
		
		for pubIter in publishers:
			pub = pubIter
			
			if(isinstance(pub,StudioController)):
					pub = pub._model
				
			self._model.publishers.add(pub)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_publishers_remove(self,publishers):
		try:
			iter(publishers)
		except TypeError:
			publishers = [publishers]
		
		for pubIter in publishers:
			pub = pubIter
			
			if(isinstance(pub,StudioController)):
					pub = pub._model
				
			self._model.publishers.remove(pub)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def update_boxart(self,boxart):
		if(isinstance(boxart,File) or isinstance(boxart,StringIO) or isinstance(boxart,file)):
			boxart = boxart.read()
		
		self._model.boxart.generate_temporary_file(boxart)
	
	@assertStatus(GameLocalController.STATUS_GAME_LOCAL_FOUND)
	@desertStatus(STATUS_GAME_EDIT_COMMITED)
	def commit(self):
		self._editStateModel = VGListEditState.objects.create(
			content_object=self._model,
			change=EditState.get_diff_from_objects(self._model_original,self._model),
			user=LoggedVGListUserController(self.request)._model);
		
		self.status|=EditGameLocalController.STATUS_GAME_EDIT_COMMITED

def edit_game(
		request,
		game,
		name=None,
		local_name=None,
		synopsis=None,
		synopsis_source_name=None,
		synopsis_source_link=None,
		release_date=None,
		publishers=None,
		add_publishers=None,
		remove_publishers=None,
		developers=None,
		add_developers=None,
		remove_developers=None,
		platform=None,
		boxart=None,
		**kwargs):
	'''
		Return Codes:
			0. Game created successfully:
				Returns GameLocal object
			
			1. Game created with some errors:
				Returns (GameLocal,(field,error_code,extra_return)+)
					'name':
						1. Name is empty
							Returns None
					'local_name':
						1. Local name is empty
							Returns None
					'release_date':
						1. Date is not valid
							Returns None
					'synopsis_source_link':
						1. Not an valid URL
							Returns None
					'publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'add_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher added successfully:
									Returns None
								1. publisher not found:
									Returns None
					'remove_publishers':
						1. Errors found
							Returns (error_code,return):
								0. publisher removed successfully:
									Returns None
								1. publisher not found:
									Returns None
					'platform':
						1. Platform not found
							Returns None
					'developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'add_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer added successfully:
									Returns None
								1. Developer not found:
									Returns None
					'remove_developers':
						1. Errors found
							Returns (error_code,return):
								0. Developer removed successfully:
									Returns None
								1. Developer not found:
									Returns None
					'boxart':
						1. Image not valid
							Returns None
			
			2. User not logged in:
				Returns None
			
			3. User doesn't have an VGList account:
				Returns None
			
			4. Game not found:
				Returns None
	'''
	
	user = get_logged_vglistuser(request)
	
	if(user[0]==1):
		return (2,None)
	elif(user[0]==2):
		return (3,None)
	
	user = user[1]
	
	game = get_game(request,game)
	
	if(game[0]!=0):
		return (4,None)
	
	game = game[1]
	
	errors = []
	
	if(name != None):
		if(not name):
			errors.append(('name',1,None))
		else:
			game.game_global.name = name
	
	if(synopsis != None):
		game.game_global.synopsis = synopsis
	
	if(synopsis_source_name != None):
		game.game_global.synopsis_source_name = synopsis_source_name
	
	if(synopsis_source_link != None):
		if(not synopsis_source_link.startswith(("http://","https://"))):
			synopsis_source_link = "http://" + synopsis_source_link
		synopsis_source_link = urlparse(synopsis_source_link)
		if(not ('.' in synopsis_source_link.netloc)):
			errors.append(('synopsis_source_link',1,None))
		else:
			game.game_global.synopsis_source_link = synopsis_source_link.geturl()
	
	if(local_name != None):
		if(not local_name):
			errors.append(('local_name',1,None))
		else:
			game.name = local_name
	
	if(release_date != None):
		if(isinstance(release_date,date)):
			game.release_date = release_date
		elif(not release_date or release_date == "0000-00-00"):
			game.release_date = None
		else:
			try:
				game.release_date = InaccurateDateField().get_value(release_date)
			except:
				errors.append(('release_date',1,None))
	
	if(publishers != None):
		publishers_ret = []
		ok=0
		try:
			iter(publishers)
		except TypeError:
			publishers = [publishers]
		
		game.publishers.clear()
		
		for dev in publishers:
			if(not isinstance(dev,Studio)):
				try:
					dev = Studio.objects.get(pk=dev)
				except Studio.DoesNotExist:
					publishers_ret.append((1,None))
					ok=1
					continue
			game.publishers.add(dev)
			publishers_ret.append((0,None))
		
		if(ok):
			errors.append(('publishers',ok,publishers_ret))
	else:
		if(add_publishers != None):
			add_publishers_ret = []
			ok=0
			try:
				iter(add_publishers)
			except TypeError:
				add_publishers = [add_publishers]
			
			for dev in add_publishers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						add_publishers_ret.append((1,None))
						ok=1
						continue
				game.publishers.add(dev)
				add_publishers_ret.append((0,None))
			
			if(ok):
				errors.append(('add_publishers',ok,add_publishers_ret))
		
		if(remove_publishers != None):
			remove_publishers_ret = []
			ok=0
			try:
				iter(remove_publishers)
			except TypeError:
				remove_publishers = [remove_publishers]
			
			for dev in remove_publishers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						remove_publishers_ret.append((1,None))
						ok=1
						continue
				game.publishers.remove(dev)
				remove_publishers_ret.append((0,None))
			
			if(ok):
				errors.append(('remove_publishers',ok,remove_publishers_ret))
	
	if(platform != None):
		if(isinstance(platform,Platform)):
			game.console = platform
		else:
			platform = get_platform(request,platform)
			if(platform[0] == 1):
				errors.append(('platform',1,None))
			else:
				game.console = platform[1]
	
	if(developers != None):
		developers_ret = []
		ok=0
		try:
			iter(developers)
		except TypeError:
			developers = [developers]
		
		game.developers.clear()
		
		for dev in developers:
			if(not isinstance(dev,Studio)):
				try:
					dev = Studio.objects.get(pk=dev)
				except Studio.DoesNotExist:
					developers_ret.append((1,None))
					ok=1
					continue
			game.developers.add(dev)
			developers_ret.append((0,None))
		
		if(ok):
			errors.append(('developers',ok,developers_ret))
	else:
		if(add_developers != None):
			add_developers_ret = []
			ok=0
			try:
				iter(add_developers)
			except TypeError:
				add_developers = [add_developers]
			
			for dev in add_developers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						add_developers_ret.append((1,None))
						ok=1
						continue
				game.developers.add(dev)
				add_developers_ret.append((0,None))
			
			if(ok):
				errors.append(('add_developers',ok,add_developers_ret))
		
		if(remove_developers != None):
			remove_developers_ret = []
			ok=0
			try:
				iter(remove_developers)
			except TypeError:
				remove_developers = [remove_developers]
			
			for dev in remove_developers:
				if(not isinstance(dev,Studio)):
					try:
						dev = Studio.objects.get(pk=dev)
					except Studio.DoesNotExist:
						remove_developers_ret.append((1,None))
						ok=1
						continue
				game.developers.remove(dev)
				remove_developers_ret.append((0,None))
			
			if(ok):
				errors.append(('remove_developers',ok,remove_developers_ret))
	
	if(boxart != None):
		if(isinstance(boxart,File) or isinstance(boxart,StringIO) or isinstance(boxart,file)):
			boxart = boxart.read()
		if(not game.save_images_from_raw(boxart)):
			errors.append(('boxart',1,None))
	
	game.game_global.save()
	game.save()
	
	if(len(errors)):
		return (1,game,errors)
	else:
		return (0,game,[])
