from django.contrib import admin
from django import forms
from django.contrib.contenttypes.generic import GenericTabularInline

from vglist.models import *
from django.core.files.uploadedfile import InMemoryUploadedFile

class GameLocalChangeForm(forms.ModelForm):
	boxart = forms.ImageField(label='Boxart',required=False)
	
	def save(self, commit=True):
		user = super(GameLocalChangeForm, self).save(commit=False)
		
		boxart = self.cleaned_data.get("boxart")
		
		if(isinstance(boxart,InMemoryUploadedFile)):
			user.save_images_from_raw(boxart.read())
		elif(boxart==False):
			user.boxart = None
	
		if commit:
			user.save()
		return user
	
	class Meta:
		model = GameLocal

class GenericCommentInline(GenericTabularInline):
	model = VGListComment
	extra = 3

class GameLocalAdmin(admin.ModelAdmin):
	inlines = [GenericCommentInline]
	form = GameLocalChangeForm
	
	ordering = ('game_global','local_id')
	list_display = ('__unicode__','game_global','local_id')

class PlatformAdmin(admin.ModelAdmin):
	inlines = [GenericCommentInline]

class StudioAdmin(admin.ModelAdmin):
	inlines = [GenericCommentInline]

class VGListUserAdmin(admin.ModelAdmin):
	inlines = [GenericCommentInline]

admin.site.register(GameGlobal)
admin.site.register(GameLocal,GameLocalAdmin)
admin.site.register(Studio,StudioAdmin)
admin.site.register(Platform,PlatformAdmin)
admin.site.register(VGListUser,VGListUserAdmin)
admin.site.register(VGListComment)
admin.site.register(ListEntry)
admin.site.register(Friendship)
admin.site.register(VGListStaff)
admin.site.register(News)
admin.site.register(VGListEditState)
