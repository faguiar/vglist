from django.template import RequestContext, loader, Template
from django.http import *
from django.shortcuts import render, render_to_response
from django.core.urlresolvers import reverse

from vglist import controller
from people import controller as people_controller
import people
from vglist.models import *
from vglist import values
from extensions import controller as extcontroller

import json
import math

def home(request):
	loggedka = controller.LoggedVGListUserController(request)
	
	if(loggedka.status == loggedka.STATUS_USER_NOT_FOUND):
		template = loader.get_template('web/vglist/home.html')
		news = controller.NewsSearchController(request)
		news_home = extcontroller.SearchPaginator(news, 0, 5)
		context = RequestContext(request, {
			'user': request.user,
			'statistics': controller.get_global_statistics(request,[
				'vglistuser_count',
				'gameglobal_count',
				'gamelocal_count',
				'studio_count',
				'platform_count',
				'listentry_count',
				'vglistcomment_count',
				'vglistuser_online_count',
			])[1],
			'news': news_home,
			'msg': request.GET.get('msg',None),
		})
		return HttpResponse(template.render(context))
	
	else:
		return HttpResponseRedirect(reverse('vglist.views.mypanel'))
	
	return HttpResponseRedirect(reverse('vglist.views.not_found'))

def mypanel(request):
	loggedka = controller.LoggedVGListUserController(request)
	if(loggedka.status == loggedka.STATUS_USER_FOUND):
		template = loader.get_template('web/vglist/mypanel.html')
		news = controller.NewsSearchController(request)
		news_home = extcontroller.SearchPaginator(news, 0, 5)

		context = RequestContext(request, {
			'profile': loggedka,
			'user': request.user,
			'statistics': controller.get_global_statistics(request,[
				'vglistuser_count',
				'gameglobal_count',
				'gamelocal_count',
				'studio_count',
				'platform_count',
				'listentry_count',
				'vglistcomment_count',
				'vglistuser_online_count',
			])[1],
			'news': news_home,
		})
		
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.home'))

def login(request):
	if(request.method=='POST'):
		res = controller.login(
			request,
			request.POST.get('username',''),
			request.POST.get('password',''),
		)
		
		if(res[0]==0):
			return HttpResponseRedirect(reverse('vglist.views.home'))
		elif(res[0]==2):
			return HttpResponseRedirect(reverse('vglist.views.login')+'?e=2&email=%s'%(res[1].email))
		else:
			return HttpResponseRedirect(reverse('vglist.views.login')+'?e=1')
	else:
		template = loader.get_template('web/vglist/login.html')
		
		context = RequestContext(request, {
			'user': request.user,
			'error': request.GET.get('e',None),
			'email': request.GET.get('email',''),
			'msg': request.GET.get('msg',None),
		})
		
		return HttpResponse(template.render(context))

def logout(request):
	people_controller.logout(request)
	
	return HttpResponseRedirect(reverse('vglist.views.home'))

def register(request):
	if(request.method=='POST'):
		if(request.POST.get('password',True)!=request.POST.get('confirm_password',False)):
			return HttpResponseRedirect(reverse('vglist.views.register')+'?e=-1')
		
		can_receive_mail = request.POST.get('can_receive_mail',None)

		if(can_receive_mail == 'True'):
			can_receive_mail = True

		elif(can_receive_mail == 'False'):
			can_receive_mail = False

		else:
			can_receive_mail = None

		res = controller.register(
			request,
			request.POST.get('email',''),
			request.POST.get('username',''),
			request.POST.get('password',''),
			can_receive_mail,
		)
		
		if(res[0]==0):
			return HttpResponseRedirect(reverse('vglist.views.home')+'?msg=activation_sent')
		else:
			return HttpResponseRedirect(reverse('vglist.views.register')+('?e=%s' % (res[0])))
	else:
		template = loader.get_template('web/vglist/register.html')
		
		context = RequestContext(request, {
			'user': request.user,
			'error': request.GET.get('e',None),
		})
		
		return HttpResponse(template.render(context))

def resend_activation_email(request):
	if(request.GET.get('email','')):
		res = people_controller.resend_activation_email(request,request.GET.get('email',''))
		
		if(res[0] == 0 or res[0] == 1):
			return HttpResponseRedirect(reverse('vglist.views.home')+'?msg=activation_sent')
		elif(res[0] == 2):
			return HttpResponseRedirect(reverse('vglist.views.home')+'?msg=already_active')
		elif(res[0] == 3):
			return HttpResponseRedirect(reverse('vglist.views.home')+'?msg=user_not_found')
	
	return HttpResponseRedirect(reverse('vglist.views.home'))

######### PANEL ZONE
def panel(request):
	if(controller.EditNewsController.can(request)):
		template = loader.get_template('web/vglist/panel.html')	
		context = RequestContext(request, {
			'user': request.user,
			'news': controller.NewsSearchController(request, published=True),
		})
		
		return HttpResponse(template.render(context))
		
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def news_drafts(request):
	if(controller.EditNewsController.can(request)):
		template = loader.get_template('web/vglist/drafts.html')
		context = RequestContext(request, {
			'user': request.user,
			'drafts': controller.NewsSearchController(request, published=False, own=True)
			})
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def create_news(request):
	if(controller.CreateNewsController.can(request)):
		if(request.method == "POST"):
			new_news = controller.CreateNewsController(request)
			
			new_news.update_title(request.POST.get('title', None))
			new_news.update_body(request.POST.get('body',None))
			
			if(request.POST.get('saveandpublish',None)):
				if(new_news.can_be_published()):
					new_news.publish_now()
			
			return HttpResponseRedirect(reverse('vglist.views.panel'))
		else:
			template = loader.get_template('web/vglist/create_news.html')
			context = RequestContext(request, {
				'user': request.user,
				})
			return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def edit_news(request, news_id):
	if(controller.EditNewsController.can(request)):
		if(request.method == "POST"):
			new_news = controller.EditNewsController(request, news_id)
			
			new_news.update_body(request.POST.get('body',None))
			new_news.update_title(request.POST.get('title',None))
			
			if(request.POST.get('saveandpublish',None)):
				if(new_news.can_be_published()):
					new_news.publish_now()

			return HttpResponseRedirect(reverse('vglist.views.panel'))
		else:
			template = loader.get_template('web/vglist/edit_news.html')
			context = RequestContext(request, {
				'user': request.user,
				'news': controller.NewsController(request, news_id),
			})
			return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def delete_news(request, news_id):
	if(controller.EditNewsController.can(request)):
		new_news = controller.EditNewsController(request, news_id)
		new_news.throw_away()
		return HttpResponseRedirect(reverse('vglist.views.news_drafts'))
		
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

####################################################################

def edit_user(request):
	if(request.method=='POST'):
		can_receive_mail = request.POST.get('can_receive_mail',None)

		if(can_receive_mail == 'True'):
			can_receive_mail = True

		elif(can_receive_mail == 'False'):
			can_receive_mail = False

		else:
			can_receive_mail = None
		
		old_password = request.POST.get('old_password',None)
		new_password = request.POST.get('new_password',None)
		email = request.POST.get('email',None)
		
		
		if(old_password == ''):
			old_password = None
		
		if(new_password == ''):
			new_password = None
			
		if(email == ''):
			email = None
		
		res = controller.edit_user(
			request,
			old_password,
			new_password,
			request.FILES.get('avatar',None),
			request.POST.get('gender',None),
			email,
			request.POST.get('bio',None),
			can_receive_mail = request.POST.get('can_receive_mail', None),
			banner = request.FILES.get('banner',None),
		)
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.profile', kwargs={'user_username':res[1].username}))
		elif(res[0]==1):
			temps_error = ''
			for i in res[2]:
				temps_error+= '&%s=%s' % (i[0],i[1])
			return HttpResponseRedirect(reverse('vglist.views.edit_user') + ('?e=%s' % (res[0]))+temps_error)
		elif(res[0]==2):
			return HttpResponseRedirect(reverse('vglist.views.edit_user') + ('?e=%s' % (res[0])))
		elif(res[0]==3):
			return HttpResponseRedirect(reverse('vglist.views.edit_user') + ('?e=%s' % (res[0])))
			
	else:
		template = loader.get_template('web/vglist/preferences.html')
		res = controller.LoggedVGListUserController(request)
		
		if (res.status == controller.VGListUserController.STATUS_USER_FOUND):
			context = RequestContext(request, {
				'profile': res,
				'error': request.GET.get('e',None),
				'new_password_error': request.GET.get('new_password',None),
				'gender_error': request.GET.get('gender',None),
				'avatar_error': request.GET.get('avatar',None),
				'email_error': request.GET.get('email',None),
			})
			
			return HttpResponse(template.render(context))
		else:
				template = loader.get_template('web/vglist/404.html')
				
				context = RequestContext(request, {})
				
				return HttpResponseNotFound(template.render(context))

########## NEWS ZONE ##########
def view_news(request, news_id):
	res = controller.NewsController(request, news_id)
	
	template = loader.get_template('web/vglist/news.html')
	context = RequestContext(request, {
		'news': res,
		})
	return HttpResponse(template.render(context))

def view_all_news(request):
	res = controller.NewsSearchController(request)
	
	offset = request.GET.get('offset',0)
	rpp = request.GET.get('rpp', 20)
	
	paginator = extcontroller.SearchPaginator(res, offset, rpp)
	
	
	browser = extcontroller.SearchPaginatorBrowser(paginator)
	
	template = loader.get_template('web/vglist/all_news.html')
	context = RequestContext(request, {
		'result': paginator,
		'browser': browser,
		'rpp': rpp,
		})
		
	return HttpResponse(template.render(context))

def add_comment_to_news(request, news_id):
	if(request.method == "POST"):
		res = controller.InteractNewsController(request, news_id)
		if(request.POST.get('text')):
			res.comment(request.POST.get('text'))
			return HttpResponseRedirect(reverse('vglist.views.view_news', kwargs={'news_id':news_id}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.view_news', kwargs={'news_id':news_id}))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

#####################################################
def game(request,global_id,local_id):
	res = controller.get_game(request,(global_id,local_id))
	
	if(res[0]==0):
		template = loader.get_template('web/vglist/game.html')
		
		context = RequestContext(request, {
			'game': res[1],
		})
		
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def search(request):
	template = loader.get_template('web/vglist/search.html')
	
	q = request.GET.get("q",None)
	key = request.GET.get("key",None)
	
	if(q == "user"):
		gender = request.GET.get("gender",None)
		if(gender == "5"):
			gender = None
		res = controller.VGListUserSearchController(request, key, gender)
		offset = request.GET.get("offset", 0)
		rpp = request.GET.get("rpp", 20)
		paginator = extcontroller.SearchPaginator(res, offset, rpp)
		browser = extcontroller.SearchPaginatorBrowser(paginator)
		
		context = RequestContext(request, {
			'user': request.user,
			'request': request,
			'q': q,
			'result': paginator,
			'rpp': rpp,
			'key': key,
			'browser': browser,
		})
	elif(q == 'studio'):
		res = controller.StudioSearchController(request, key)
		offset = request.GET.get("offset", 0)
		rpp = request.GET.get("rpp", 20)
		paginator = extcontroller.SearchPaginator(res, offset, rpp)
		browser = extcontroller.SearchPaginatorBrowser(paginator)
		
		context = RequestContext(request, {
			'user': request.user,
			'request': request,
			'q':q,
			'result':paginator,
			'rpp': rpp,
			'key': key,
			'browser': browser,
		})
	elif(q == 'platform'):
		manufacturer = request.GET.get("manufacturer", None)
		res = controller.PlatformSearchController(request, key, manufacturer)
		offset = request.GET.get("offset",0)
		rpp = request.GET.get("rpp", 20)
		paginator = extcontroller.SearchPaginator(res, offset, rpp)
		browser = extcontroller.SearchPaginatorBrowser(paginator)
		
		context = RequestContext(request, {
			'user': request.user,
			'request': request,
			'q':q,
			'result':paginator,
			'rpp': rpp,
			'key': key,
			'browser': browser,
			'manufacturer': manufacturer,
		}) 
		
	elif(q == 'game'):
		platform = request.GET.get('platform', None)
		developer = request.GET.get('developer', None)
		publisher = request.GET.get('publisher', None)

		if(platform == '0'):
			platform = None
		
		if(developer == '0'):
			developer = None
		
		if(publisher == '0'):
			publisher = None
		
		max_year = request.GET.get('max_year', None)
		max_month = request.GET.get('max_month', None)
		max_day = request.GET.get('max_day',None)
		
		if((max_day is None) or (max_day == '')):
			max_day = '31'
		if((max_month is None) or (max_month == '') or (max_month == '00')):
			max_month = '12'
		if((max_year is None) or (max_year == '')):
			max_year = '2050'
		
		print max_year
		
		max_releasedate = max_year+'-'+max_month+'-'+ "%02d" % int(max_day);
		
		min_year = request.GET.get('min_year', None)
		min_month = request.GET.get('min_month', None)
		min_day = request.GET.get('min_day',None)
		
		if((min_day is None) or (min_day == '')):
			min_day = '01'
		if((min_month is None) or (min_month == '') or (min_month == '00')):
			min_month = '01'
		if((min_year == None) or (min_year == '')):
			min_year = '1944'
					
		min_releasedate = min_year+'-'+min_month+'-'+ "%02d" % int(min_day);
		
		print "------------_\n"
		print min_releasedate
		print max_releasedate
		print "------------\n"
		
		res = controller.GameSearchController(request, key, min_releasedate, max_releasedate, publisher, developer, platform)
		offset = request.GET.get("offset",0)
		rpp = request.GET.get("rpp", 20)
		paginator = extcontroller.SearchPaginator(res, offset, rpp)
		browser = extcontroller.SearchPaginatorBrowser(paginator)
		
		context = RequestContext(request, {
			'user': request.user,
			'request': request,
			'q':q,
			'result':paginator,
			'rpp': rpp,
			'key': key,
			'browser': browser,
		}) 
		
	else:
		context = RequestContext(request, {
			'user': request.user,
			'q': q,
			'result': None,
			'key': key,
		})
			
	return HttpResponse(template.render(context))

def studio(request,studio_id):
	res = controller.get_studio(request,studio_id)
	
	if(res[0]==0):
		template = loader.get_template('web/vglist/studio.html')
		
		context = RequestContext(request, {
			'studio': res[1],
		})
		
		return HttpResponse(template.render(context))
	
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def add_comment_to_studio(request, studio_id):
	if(request.method=='POST'):
		res = controller.add_comment_to_studio(
			request,
			studio = studio_id,
			comment_text = request.POST.get('text'),
		)
		
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.studio', kwargs={'studio_id':studio_id,}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))		
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))
	
	
def platform(request, platform_id):
	res = controller.get_platform(request,platform_id)
					
	if(res[0]==0):
		template = loader.get_template('web/vglist/platform.html')
		
		context = RequestContext(request, {
			'platform': res[1],
		})
		
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def add_comment_to_platform(request, platform_id):
	if(request.method=='POST'):
		res = controller.add_comment_to_platform(
			request,
			platform = platform_id,
			comment_text = request.POST.get('text'),
		)
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.platform', kwargs={'platform_id':platform_id,}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))		
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def game_global(request,global_id):
	return HttpResponseRedirect(reverse('vglist.views.game', kwargs={'global_id':global_id, 'local_id':1}))

def add_comment_to_game(request, global_id, local_id):
	if(request.method=='POST'):
		res = controller.add_comment_to_game(
			request,
			game = (global_id, local_id),
			comment_text = request.POST.get('text'),
		)
		
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.game', kwargs={'global_id':global_id,'local_id':local_id}))			
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))		
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def user_list(request,user_username):
	res = controller.get_vglistuser(request,user_username)
	
	if(res[0]==0):
		template = loader.get_template('web/vglist/list.html')
		
		context = RequestContext(request, {
			'profile': res[1].user,
		})
		
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def profile(request,user_username):
	res = controller.VGListUserController(request,user_username)
	
	if(res.status == res.STATUS_USER_FOUND):
		template = loader.get_template('web/vglist/profile.html')
		
		context = RequestContext(request, {
			'profile': res,
		})
		
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))


def friends(request, user_username):
	res = controller.VGListUserController(request,user_username)
	
	if(res.status == res.STATUS_USER_FOUND):
		template = loader.get_template('web/vglist/friends.html')
		
		context = RequestContext(request,{
			'profile': res,
			})
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))
		
def notifications(request, user_username):
	res = controller.LoggedVGListUserController(request)
	
	if(res.status == res.STATUS_USER_FOUND):
		template = loader.get_template('web/vglist/notifications.html')
		
		context = RequestContext(request,{
			'profile': res,
			})
		return HttpResponse(template.render(context))
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))

def accept_friend_request(request, logged_user, user_username,):
	res = controller.accept_friend_request(request, user_username)
	
	if(res[0] == 0):
		return HttpResponseRedirect(reverse('vglist.views.notifications', kwargs={"user_username":logged_user}))			
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))		
		
	

def reject_friend_request(request, logged_user,  user_username):
	res = controller.reject_friend_request(request, user_username)
		
	if(res[0] == 0):
		return HttpResponseRedirect(reverse('vglist.views.notifications', kwargs={"user_username":logged_user}))			
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))		
		
		
def remove_friend(request, logged_user, user_username):
	res = controller.remove_friend(request, user_username)
	if(res[0] == 0):
		return HttpResponseRedirect(reverse('vglist.views.friends', kwargs={"user_username":logged_user}))			
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))		
		
	

def add_comment_to_profile(request, user_username):
	if(request.method=='POST'):
		res = controller.add_comment_to_profile(
			request,
			profile_user = user_username,
			comment_text = request.POST.get('text'),
		)
		
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.profile', kwargs={'user_username':user_username}))			
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))		
	else:
		return HttpResponseRedirect(reverse('vglist.views.not_found'))


def add_friend(request, user_username):
	if(request.method=='POST'):
		res = controller.add_friend(
			request,
			user = user_username,
		)
		
		if (res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.profile', kwargs={'user_username':user_username}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))
	else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))
		
			

def not_found(request):
	template = loader.get_template('web/vglist/404.html')
	
	context = RequestContext(request, {})
	
	return HttpResponseNotFound(template.render(context))

def staff(request):
	template = loader.get_template('web/vglist/staff.html')
	
	res = controller.VGListStatistics(request)
	
	context = RequestContext(request, {'stats': res,})
	
	return HttpResponse(template.render(context))

def policy(request):
	template = loader.get_template('web/vglist/policy.html')
	
	context = RequestContext(request, {})
	
	return HttpResponse(template.render(context))

def about(request):
	template = loader.get_template('web/vglist/about.html')
	
	context = RequestContext(request,{})
	
	return HttpResponse(template.render(context))

def contact(request):	
	template = loader.get_template('web/vglist/contact.html')
	
	context = RequestContext(request,{})
	return HttpResponse(template.render(context))

def add_game(request):
	if(request.method=='POST'):		
		res = controller.create_new_game(
			request,
			name = request.POST.get('name',''),
			synopsis = request.POST.get('synopsis',''),
			synopsis_source_name = request.POST.get('synopsis_source_name', None),
			synopsis_source_link = request.POST.get('synopsis_source_link', None),
		)
		
		if(res[0]==0):
			return HttpResponseRedirect(reverse('vglist.views.edit_game', kwargs={'global_id':res[1].game_global_id, 'local_id':res[1].local_id}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.add_game')+('?e=%s' % (res[0])))
	else:
		template = loader.get_template('web/vglist/add_game.html')
		
		context = RequestContext(request, {
			'user': request.user,
			'error': request.GET.get('e',None),
		})
		
		return HttpResponse(template.render(context))

def add_game_version(request, global_id):
	if(request.method=='POST'):
		res = controller.create_new_game_version(
			request,
			global_id,
			local_name = request.POST.get('name',''),
		)
		if(res[0]==0):
			return HttpResponseRedirect(reverse('vglist.views.edit_game', kwargs={'global_id':res[1].game_global_id, 'local_id':res[1].local_id}))
	else:
		template = loader.get_template('web/vglist/add_game_version.html')
		
		context = RequestContext(request, {
			'user': request.user,
			'error': request.GET.get('e',None),
		})
		
		return HttpResponse(template.render(context))

def edit_game(request, global_id, local_id):
	if(request.method=='POST'):
		
		year = request.POST.get('year', None)
		month = request.POST.get('month', None)
		day = request.POST.get('day',None)
		
		if((day is None) or (day == '')):
			day = '00'
					
		releasedate = year+'-'+month+'-'+ "%02d" % int(day);

		res = controller.edit_game(
			request,
			(global_id, local_id),
			name = request.POST.get('name', None),
			local_name = request.POST.get('local_name', None),
			release_date = releasedate,
			boxart = request.FILES.get('boxart', None),
			synopsis = request.POST.get('synopsis', None),
			synopsis_source_name = request.POST.get('synopsis_source_name', None),
			synopsis_source_link = request.POST.get('synopsis_source_link', None),
			platform = request.POST.get('platform', None),
			developers = request.POST.getlist('developers', None),
			publishers = request.POST.getlist('publishers', None),
			region = request.POST.get('region', None),
		)
		
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.game', kwargs={'global_id':global_id, 'local_id':local_id}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.game', kwargs={'global_id':global_id, 'local_id':local_id})+('?e=%s' % (res[0])))
	
	else:
		res = controller.get_game(request,(global_id, local_id))
		if(res[0] == 0):
			template = loader.get_template('web/vglist/edit_game.html')
			context = RequestContext(request, {
				'user': request.user,
				'game': res[1],
				'error': request.GET.get('e',None),
			})
			return HttpResponse(template.render(context))
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))

def add_platform(request):
	if(request.method=='POST'):		
		res = controller.add_platform(
			request,
			name = request.POST.get('name',''),
		)
		if(res[0]==0):
			return HttpResponseRedirect(reverse('vglist.views.edit_platform', kwargs={'platform_id':res[1].pk}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.add_platform')+('?e=%s' % (res[0])))
	else:
		template = loader.get_template('web/vglist/add_platform.html')
		
		context = RequestContext(request, {
			'user': request.user,
			'error': request.GET.get('e',None),
		})
	
		return HttpResponse(template.render(context))

def edit_platform(request, platform_id):
	if(request.method=='POST'):
		res = controller.edit_platform(
			request,
			platform_id,
			name = request.POST.get('name', None),
			synopsis = request.POST.get('synopsis', None),
			synopsis_source_name = request.POST.get('synopsis_source_name', None),
			synopsis_source_link = request.POST.get('synopsis_source_link', None),
			image = request.FILES.get('image', None),
			alternative_names = request.POST.getlist('alternative_names',None),
			manufacturer = request.POST.get('manufacturer', None),
		)
		
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.platform', kwargs={'platform_id':platform_id}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.platform', kwargs={'platform_id':platform_id})+('?e=%s' % (res[0])))
	
	else:
		res = controller.edit_platform(request, platform_id)
		if(res[0] == 0):
			template = loader.get_template('web/vglist/edit_platform.html')
			context = RequestContext(request, {
				'user': request.user,
				'error': request.GET.get('e',None),
				'platform': res[1],
			})
			return HttpResponse(template.render(context))
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))

def add_studio(request):
	if(request.method=='POST'):		
		res = controller.add_studio(
			request,
			name = request.POST.get('name',''),
		)
		if(res[0]==0):
			return HttpResponseRedirect(reverse('vglist.views.edit_studio', kwargs={'studio_id':res[1].pk}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.add_studio')+('?e=%s' % (res[0])))
	else:
		template = loader.get_template('web/vglist/add_studio.html')
		
		context = RequestContext(request, {
			'user': request.user,
			'error': request.GET.get('e',None),
		})
	
		return HttpResponse(template.render(context))

def edit_studio(request, studio_id):
	if(request.method=='POST'):
		res = controller.edit_studio(
			request,
			studio_id,
			name = request.POST.get('name', None),
			synopsis = request.POST.get('synopsis', None),
			synopsis_source_name = request.POST.get('synopsis_source_name', None),
			synopsis_source_link = request.POST.get('synopsis_source_link', None),
			image = request.FILES.get('image', None),
		)
		
		if(res[0] == 0):
			return HttpResponseRedirect(reverse('vglist.views.studio', kwargs={'studio_id':studio_id}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.studio', kwargs={'studio_id':studio_id})+('?e=%s' % (res[0])))
	
	else:
		res = controller.edit_studio(request, studio_id)
		if(res[0] == 0):
			template = loader.get_template('web/vglist/edit_studio.html')
			context = RequestContext(request, {
				'user': request.user,
				'error': request.GET.get('e',None),
				'studio': res[1],
			})
			return HttpResponse(template.render(context))
		else:
			return HttpResponseRedirect(reverse('vglist.views.not_found'))

def add_list_entry(request, global_id, local_id):
	version_id = request.POST.get('version')
	
	if(request.method=='POST'): 	
		res = controller.add_list_entry(
			request,
			(global_id, version_id),
			status = request.POST.get('status',None),
			score = request.POST.get('score',None),
		)

		if(res[0]==0):
			return HttpResponseRedirect(reverse('vglist.views.game', kwargs={'global_id':global_id, 'local_id':local_id}))
		else:
			return HttpResponseRedirect(reverse('vglist.views.game', kwargs={'global_id':global_id, 'local_id':local_id}))

def edit_list_entry(request, global_id, local_id):
	if(request.method=='POST'):
		res = controller.edit_list_entry(
			request,
			(global_id,local_id),
			score = request.POST.get('score',None),
			status = request.POST.get('status',None),
			
		)
		
		return HttpResponseRedirect(reverse('vglist.views.user_list', kwargs={'user_username':request.user}))
		
	else:
		return HttpResponseBadRequest()

def remove_list_entry(request, global_id, local_id):
	if(request.method == 'POST'):
		res = controller.remove_list_entry(
			request,
			(global_id, local_id)
		)
		
		return HttpResponseRedirect(reverse('vglist.views.user_list', kwargs={'user_username':request.user}))
	
	else:
		return HttpResponseBadRequest()
		
		
def studios_by_prefix(request):
	if(request.method != "GET"):
		return HttpResponseBadRequest(json.dumps({'status_code':'BAD REQUEST'}))
	
	res = controller.get_studios_by_prefix(request,request.GET.get('prefix',''))
	
	if(res[0]==2):
		return HttpResponse(json.dumps({'status_code':'ERROR', 'result':{
			'error': (1,'SMALL PREFIX')
		}},indent=4), content_type="application/json")
	elif(res[0]==1 or res[0]==0):
		return HttpResponse(json.dumps(
			{
				'status_code':('OK' if res[0]==0 else 'NO RESULTS'),
				'result': list(res[1].order_by('name').values('pk','name'))
			},indent=4), content_type="application/json")

def platforms_by_prefix(request):
	if(request.method != "GET"):
		return HttpResponseBadRequest(json.dumps({'status_code':'BAD REQUEST'}))
	
	res = controller.get_platforms_by_prefix(request,request.GET.get('prefix',''))
	
	if(res[0]==2):
		return HttpResponse(json.dumps({'status_code':'ERROR', 'result':{
			'error': (1,'SMALL PREFIX')
		}},indent=4), content_type="application/json")
	elif(res[0]==1 or res[0]==0):
		return HttpResponse(json.dumps(
			{
				'status_code':('OK' if res[0]==0 else 'NO RESULTS'),
				'result': list(res[1].order_by('name').values('pk','name'))
			},indent=4), content_type="application/json")

# Recovery password views

def request_recover_password(request):

	template = loader.get_template('web/vglist/request_recover_password.html')

	context = RequestContext(request, {
		'hash': request.GET.get("hash",None),
	})

	if(request.method == "GET" and request.GET.get("param",None) == "resend"):
		context['controller_return'] = 4

	if(request.method == "POST"):
		context['controller_return'] = people_controller.RequestRecoverPasswordController(request,request.POST.get("email",None)).status
	
	return HttpResponse(template.render(context))

def recover_password(request):

	template = loader.get_template('web/vglist/recover_password.html')


	# came here without a hash to change password
	# lets send him to login page
	his_hash = request.GET.get("hash", None)
	if not his_hash:
		return HttpResponseRedirect(reverse('vglist.views.login'))


	context = RequestContext(request, {
		'hash': his_hash,
	})

	if(request.method == "POST"):
		req_hash = request.POST.get("hash", None)
		req_newpass = request.POST.get("password", None)
		controller = people_controller.RecoverPasswordController(request, req_hash, req_newpass)
		context['controller_return'] = controller.status
		
	
	
	
	return HttpResponse(template.render(context))
