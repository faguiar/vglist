from extensions.choices import IntegerChoices

LIST_STATUS = IntegerChoices([
	'Playing',
	'Wished',
	'Completed',
	'Not playing',
	'On hold',
	'Mastered',
	'Dropped',
])

ALLOWED_RESULTS_PER_PAGE = sorted([
	5,
	20,
	50,
	100,
])
