# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

from django.contrib.contenttypes.models import ContentType

class Migration(DataMigration):
	
	needed_by = (
        ("people", "0013_auto__chg_field_user_username__chg_field_user_email__add_index_user_em"),
    )

	def forwards(self, orm):
		for l in orm.ListEntry.objects.all():
			l.user_id = orm['people.User'].objects.get(email=l.user_id).username
			l.save()
		
		for c in orm.VGListComment.objects.all():
			c.commenter_id = orm['people.User'].objects.get(email=c.commenter_id).username
			
			if(c.content_type == ContentType.objects.get_for_model(orm.VGListUser)):
				c.object_id = orm['people.User'].objects.get(email=c.object_id).username
			
			c.save()
		
		for u in list(orm.VGListUser.objects.all()):
			user_id_bkp = u.user_id
			u.delete()
			u.user_id = orm['people.User'].objects.get(email=user_id_bkp).username
			u.save()

	def backwards(self, orm):
		for l in orm.ListEntry.objects.all():
			l.user_id = orm['people.User'].objects.get(username=l.user_id).email
			l.save()
		
		for c in orm.VGListComment.objects.all():
			c.commenter_id = orm['people.User'].objects.get(username=c.commenter_id).email
			
			if(c.content_type == ContentType.objects.get_for_model(orm.VGListUser)):
				c.object_id = orm['people.User'].objects.get(username=c.object_id).email
			
			c.save()
		
		for u in list(orm.VGListUser.objects.all()):
			user_id_bkp = u.user_id
			u.delete()
			u.user_id = orm['people.User'].objects.get(username=user_id_bkp).email
			u.save()

	models = {
		u'contenttypes.contenttype': {
			'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
			'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
			'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
		},
		u'people.comment': {
			'Meta': {'object_name': 'Comment'},
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'post_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
			'text': ('django.db.models.fields.TextField', [], {})
		},
		u'people.user': {
			'Meta': {'object_name': 'User'},
			'avatar': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
			'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'primary_key': 'True'}),
			'gender': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
			'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
			'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
			'join_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
			'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
			'last_online': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
			'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
			'username': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '32'})
		},
		u'vglist.gameglobal': {
			'Meta': {'object_name': 'GameGlobal'},
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
			'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
		},
		u'vglist.gamelocal': {
			'Meta': {'ordering': "['game_global', 'local_id']", 'unique_together': "[['game_global', 'local_id']]", 'object_name': 'GameLocal', 'index_together': "[['game_global', 'local_id']]"},
			'boxart': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
			'console': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.Platform']", 'null': 'True', 'blank': 'True'}),
			'developers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'developed_games'", 'blank': 'True', 'to': u"orm['vglist.Studio']"}),
			'game_global': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.GameGlobal']"}),
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'local_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
			'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
			'publisher': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'published_games'", 'null': 'True', 'to': u"orm['vglist.Studio']"}),
			'release_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
		},
		u'vglist.listentry': {
			'Meta': {'unique_together': "[['user', 'game']]", 'object_name': 'ListEntry'},
			'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.GameLocal']"}),
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '3', 'decimal_places': '1', 'blank': 'True'}),
			'status': ('django.db.models.fields.IntegerField', [], {}),
			'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.VGListUser']"})
		},
		u'vglist.platform': {
			'Meta': {'object_name': 'Platform'},
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'manufacturer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.Studio']", 'null': 'True', 'blank': 'True'}),
			'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
			'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
		},
		u'vglist.studio': {
			'Meta': {'object_name': 'Studio'},
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
			'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
		},
		u'vglist.vglistcomment': {
			'Meta': {'object_name': 'VGListComment'},
			'comment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['people.Comment']", 'unique': 'True'}),
			'commenter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.VGListUser']"}),
			'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
			u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
			'object_id': ('django.db.models.fields.CharField', [], {'max_length': '100'})
		},
		u'vglist.vglistuser': {
			'Meta': {'object_name': 'VGListUser'},
			'bio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
			'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['people.User']", 'unique': 'True', 'primary_key': 'True'})
		}
	}

	complete_apps = ['vglist']
	symmetrical = True
