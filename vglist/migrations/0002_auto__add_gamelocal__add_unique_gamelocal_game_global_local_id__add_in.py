# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GameLocal'
        db.create_table(u'vglist_gamelocal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game_global', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vglist.GameGlobal'])),
            ('local_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'vglist', ['GameLocal'])

        # Adding unique constraint on 'GameLocal', fields ['game_global', 'local_id']
        db.create_unique(u'vglist_gamelocal', ['game_global_id', 'local_id'])

        # Adding index on 'GameLocal', fields ['game_global', 'local_id']
        db.create_index(u'vglist_gamelocal', ['game_global_id', 'local_id'])

        # Adding model 'GameGlobal'
        db.create_table(u'vglist_gameglobal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('synopsis', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'vglist', ['GameGlobal'])


    def backwards(self, orm):
        # Removing index on 'GameLocal', fields ['game_global', 'local_id']
        db.delete_index(u'vglist_gamelocal', ['game_global_id', 'local_id'])

        # Removing unique constraint on 'GameLocal', fields ['game_global', 'local_id']
        db.delete_unique(u'vglist_gamelocal', ['game_global_id', 'local_id'])

        # Deleting model 'GameLocal'
        db.delete_table(u'vglist_gamelocal')

        # Deleting model 'GameGlobal'
        db.delete_table(u'vglist_gameglobal')


    models = {
        u'vglist.gameglobal': {
            'Meta': {'object_name': 'GameGlobal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'vglist.gamelocal': {
            'Meta': {'ordering': "['game_global', 'local_id']", 'unique_together': "[['game_global', 'local_id']]", 'object_name': 'GameLocal', 'index_together': "[['game_global', 'local_id']]"},
            'game_global': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.GameGlobal']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['vglist']