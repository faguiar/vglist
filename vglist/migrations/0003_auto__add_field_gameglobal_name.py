# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'GameGlobal.name'
        db.add_column(u'vglist_gameglobal', 'name',
                      self.gf('django.db.models.fields.CharField')(default='name', max_length=128),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'GameGlobal.name'
        db.delete_column(u'vglist_gameglobal', 'name')


    models = {
        u'vglist.gameglobal': {
            'Meta': {'object_name': 'GameGlobal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'vglist.gamelocal': {
            'Meta': {'ordering': "['game_global', 'local_id']", 'unique_together': "[['game_global', 'local_id']]", 'object_name': 'GameLocal', 'index_together': "[['game_global', 'local_id']]"},
            'game_global': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.GameGlobal']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['vglist']