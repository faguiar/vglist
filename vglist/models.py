from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from vglist import values
from decimal import Decimal

from extensions.choices import IntegerChoices
from extensions.models import EditState

from PIL import Image
from StringIO import StringIO
from django.db.models.fields.files import ImageFieldFile, FileField

from django_inaccurate_date.fields import *
from django.utils import timezone

from django.db.models.signals import *
from django.dispatch import receiver

import os
import string
import random
import re

from unidecode import unidecode

from django.conf import settings

class VGListUser(models.Model):
	BANNER_SUBPATH = 'user/banner/'
	BANNER_HASH_LETTERS = re.sub('[!vglistVGLIST]', '', string.ascii_letters+string.digits);
	
	user = models.OneToOneField('people.User', primary_key=True)
	
	bio = models.TextField(blank=True)
	
	comments = generic.GenericRelation('VGListComment')
	
	can_receive_mail = models.BooleanField(default=False)
	
	banner = models.ImageField(upload_to=os.path.join(settings.MEDIA_ROOT,BANNER_SUBPATH), default='', blank=True)
	
	def save_banner_from_raw(self,raw):
		filename = '%s_%s.jpg' % (self.pk,''.join(random.choice(self.BANNER_HASH_LETTERS) for _ in range(32)))
		subpath = self.BANNER_SUBPATH
		
		try:
			img = Image.open(StringIO(raw)).convert("RGB")
		except IOError as e:
			return False
		
		if(self.banner):
			self.banner.delete()
		
		img.thumbnail((1500,475),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(settings.MEDIA_ROOT,subpath),filename), "JPEG", quality=80, optimize=True, progressive=True)
		self.banner = os.path.join(subpath,filename)
		
		return True
	
	def __unicode__(self):
		return self.user.__unicode__()

class ListEntry(models.Model):
	user = models.ForeignKey('VGListUser')
	game = models.ForeignKey('GameLocal')
	status = models.IntegerField(choices=values.LIST_STATUS.choices())
	score = models.DecimalField(null=True,blank=True,decimal_places=1,max_digits=4)
	
	def __unicode__(self):
		return '%s - %s' % (self.user.__unicode__(),self.game.__unicode__())
	
	class Meta:
		unique_together = [
			["user","game"]
		]

class Studio(models.Model):
	IMAGE_SUBPATH = 'studio/image/'
	
	name = models.CharField(max_length=128)
	search_ready_name = models.CharField(max_length=256, default='')
	
	synopsis = models.TextField(blank=True)
	synopsis_source_name = models.CharField(max_length=128, default="")
	synopsis_source_link = models.URLField(default="")
	
	image = models.ImageField(upload_to=os.path.join(settings.MEDIA_ROOT,IMAGE_SUBPATH), default='', blank=True)
	
	comments = generic.GenericRelation('VGListComment')
	
	alternative_names = generic.GenericRelation('extensions.AlternativeName')
	
	def short_name(self):
		return min([self.name] + list([i.name for i in self.alternative_names.all()]))
	
	def regular(self):
		if(self.image):
			filename = 'r_%s' % (os.path.basename(self.image.name))
			path = os.path.join(os.path.dirname(self.image.name),'regular/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def thumbnail(self):
		if(self.image):
			filename = 't_%s' % (os.path.basename(self.image.name))
			path = os.path.join(os.path.dirname(self.image.name),'thumbs/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def save_images_from_raw(self,raw):
		filename = '%s.png' % (self.pk)
		subpath = self.IMAGE_SUBPATH
		
		try:
			img = Image.open(StringIO(raw)).convert("RGBA")
		except IOError as e:
			return False
		
		img.thumbnail((1024,1024),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(settings.MEDIA_ROOT,subpath),filename), 'PNG')
		self.image = os.path.join(subpath,filename)
		
		img.thumbnail((360,360),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,subpath),'regular/'),'r_%s' % (filename)), 'PNG')
		
		img.thumbnail((80,80),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,subpath),'thumbs/'),'t_%s' % (filename)), 'PNG')
		
		return True
	
	def save(self,*args,**kwargs):
		self.search_ready_name = unidecode(self.name)
		
		super(self.__class__,self).save(*args,**kwargs)
	
	def __unicode__(self):
		return self.name

@receiver(pre_delete, sender=Studio, dispatch_uid='remove_studio_image_pre_delete')
def remove_studio_image_pre_delete(sender, instance, using, **kwargs):
	if(instance.thumbnail() and os.path.isfile(instance.thumbnail().path)):
		os.remove(instance.thumbnail().path);
	if(instance.regular() and os.path.isfile(instance.regular().path)):
		os.remove(instance.regular().path)
	if(instance.image and os.path.isfile(instance.image.path)):
		instance.image.delete();
	instance.boxart = None
	instance.save()

class Platform(models.Model):
	IMAGE_SUBPATH = 'platform/image/'
	
	name = models.CharField(max_length=128)
	search_ready_name = models.CharField(max_length=256, default='')
	
	synopsis = models.TextField(blank=True)
	synopsis_source_name = models.CharField(max_length=128, default="")
	synopsis_source_link = models.URLField(default="")
	
	manufacturer = models.ForeignKey('Studio', null=True, blank=True)
	
	image = models.ImageField(upload_to=os.path.join(settings.MEDIA_ROOT,IMAGE_SUBPATH), default='', blank=True)
	
	comments = generic.GenericRelation('VGListComment')
	
	alternative_names = generic.GenericRelation('extensions.AlternativeName')
	
	def short_name(self):
		return min([self.name] + list([i.name for i in self.alternative_names.all()]))
	
	def regular(self):
		if(self.image):
			filename = 'r_%s' % (os.path.basename(self.image.name))
			path = os.path.join(os.path.dirname(self.image.name),'regular/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def thumbnail(self):
		if(self.image):
			filename = 't_%s' % (os.path.basename(self.image.name))
			path = os.path.join(os.path.dirname(self.image.name),'thumbs/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def save_images_from_raw(self,raw):
		filename = '%s.jpg' % (self.pk)
		subpath = self.IMAGE_SUBPATH
		
		try:
			raw = Image.open(StringIO(raw)).convert("RGBA")
			img = Image.new("RGB",(raw.size[0],raw.size[1]),(255,255,255))
			img.paste(raw,(0,0),raw)
		except IOError:
			return False
		
		img.thumbnail((1024,1024),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(settings.MEDIA_ROOT,subpath),filename), 'JPEG')
		self.image = os.path.join(subpath,filename)
		
		img.thumbnail((360,360),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,subpath),'regular/'),'r_%s' % (filename)), 'JPEG')
		
		img.thumbnail((80,80),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,subpath),'thumbs/'),'t_%s' % (filename)), 'JPEG')
		
		return True
	
	def save(self,*args,**kwargs):
		self.search_ready_name = unidecode(self.name)
		
		super(self.__class__,self).save(*args,**kwargs)
	
	def __unicode__(self):
		return self.name

@receiver(pre_delete, sender=Platform, dispatch_uid='remove_platform_image_pre_delete')
def remove_platform_image_pre_delete(sender, instance, using, **kwargs):
	if(instance.thumbnail() and os.path.isfile(instance.thumbnail().path)):
		os.remove(instance.thumbnail().path);
	if(instance.regular() and os.path.isfile(instance.regular().path)):
		os.remove(instance.regular().path)
	if(instance.image and os.path.isfile(instance.image.path)):
		instance.image.delete();
	instance.boxart = None
	instance.save()

class GameGlobal(models.Model):
	name = models.CharField(max_length=128)
	search_ready_name = models.CharField(max_length=256, default='')
	
	synopsis = models.TextField(blank=True)
	synopsis_source_name = models.CharField(max_length=128, default="")
	synopsis_source_link = models.URLField(default="")
	
	def score(self):
		return ListEntry.objects.filter(game__game_global=self.pk).aggregate(models.Avg('score'))['score__avg']
	
	def save(self,*args,**kwargs):
		self.search_ready_name = unidecode(self.name)
		
		super(self.__class__,self).save(*args,**kwargs)
	
	def __unicode__(self):
		return "Game %s. %s" % (self.id,self.name)

class GameLocal(models.Model):
	BOXART_SUBPATH = 'game/boxart/'
	
	game_global = models.ForeignKey('GameGlobal')
	local_id = models.IntegerField(default=0)
	
	name = models.CharField(max_length=128)
	search_ready_name = models.CharField(max_length=256, default='')
	release_date = InaccurateDateField(null=True, blank=True)
	
	publishers = models.ManyToManyField('Studio', related_name = 'published_games', null=True, blank=True)
	developers = models.ManyToManyField('Studio', related_name = 'developed_games', blank=True)
	
	boxart = models.ImageField(upload_to=os.path.join(settings.MEDIA_ROOT,BOXART_SUBPATH), default='', blank=True)
	
	console = models.ForeignKey('Platform', null=True, blank=True)
	
	comments = generic.GenericRelation('VGListComment')
	
	def score(self):
		return self.listentry_set.aggregate(models.Avg('score'))['score__avg']
	
	def high_definition(self):
		if(self.boxart):
			filename = 'hd_%s' % (os.path.basename(self.boxart.name))
			path = os.path.join(os.path.dirname(self.boxart.name),'high_definition/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def regular(self):
		if(self.boxart):
			filename = 'r_%s' % (os.path.basename(self.boxart.name))
			path = os.path.join(os.path.dirname(self.boxart.name),'regular/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def thumbnail(self):
		if(self.boxart):
			filename = 't_%s' % (os.path.basename(self.boxart.name))
			path = os.path.join(os.path.dirname(self.boxart.name),'thumbs/')
			
			return ImageFieldFile(instance=None, field=FileField(),name=os.path.join(path,filename))
		else:
			return ImageFieldFile(instance=None, field=FileField(),name=None)
	
	def save_images_from_raw(self,raw):
		filename = '%s_%s.jpg' % (self.game_global_id,self.local_id)
		
		try:
			raw = Image.open(StringIO(raw)).convert("RGBA")
			img = Image.new("RGB",(raw.size[0],raw.size[1]),(255,255,255))
			img.paste(raw,(0,0),raw)
		except IOError:
			return False
		
		img.thumbnail((4096,4096),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(settings.MEDIA_ROOT,self.BOXART_SUBPATH),filename), 'JPEG')
		self.boxart = os.path.join(self.BOXART_SUBPATH,filename)
		
		img.thumbnail((1024,1024),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,self.BOXART_SUBPATH),'high_definition/'),'hd_%s' % (filename)), 'JPEG')
		
		img.thumbnail((360,360),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,self.BOXART_SUBPATH),'regular/'),'r_%s' % (filename)), 'JPEG')
		
		img.thumbnail((80,80),Image.ANTIALIAS)
		img.save(os.path.join(os.path.join(os.path.join(settings.MEDIA_ROOT,self.BOXART_SUBPATH),'thumbs/'),'t_%s' % (filename)), 'JPEG')
		
		return True
	
	def save(self,*args,**kwargs):
		if(not self.local_id):
			local_max = GameLocal.objects.filter(game_global=self.game_global_id).aggregate(models.Max('local_id'))['local_id__max']
			self.local_id = local_max+1 if local_max else 1
		
		self.search_ready_name = unidecode(self.name)
		
		super(self.__class__,self).save(*args,**kwargs)
	
	def __unicode__(self):
		return "%s.%s. %s" % (self.game_global.__unicode__(), self.local_id, self.name)
	
	class Meta:
		unique_together = [
			["game_global","local_id"]
		]
		
		index_together = [
			["game_global","local_id"]
		]
		
		ordering = ["game_global","local_id"]

@receiver(pre_delete, sender=GameLocal, dispatch_uid='remove_boxart_pre_delete')
def remove_boxart_pre_delete(sender, instance, using, **kwargs):
	if(instance.thumbnail() and os.path.isfile(instance.thumbnail().path)):
		os.remove(instance.thumbnail().path);
	if(instance.regular() and os.path.isfile(instance.regular().path)):
		os.remove(instance.regular().path)
	if(instance.high_definition() and os.path.isfile(instance.high_definition().path)):
		os.remove(instance.high_definition().path)
	if(instance.boxart and os.path.isfile(instance.boxart.path)):
		instance.boxart.delete();
	instance.boxart = None
	instance.save()

class VGListComment(models.Model):
	comment = models.OneToOneField('people.Comment',blank=False,null=False)
	commenter = models.ForeignKey('VGListUser',blank=False,null=False)
	
	content_type = models.ForeignKey(ContentType)
	object_id = models.CharField(max_length=100)
	content_object = generic.GenericForeignKey('content_type', 'object_id')
	
	def __unicode__(self):
		return "%s: %s" % (self.commenter.__unicode__(), self.comment.__unicode__())

class Friendship(models.Model):
	STATUS_CHOICES = IntegerChoices([
		"Pending",
		"Confirmed",
	])
	
	vglistuser_source = models.ForeignKey('VGListUser',blank=False,null=False,related_name="friends_requested")
	
	vglistuser_active = models.ForeignKey('VGListUser',blank=False,null=False,related_name="friends_passive")
	vglistuser_passive = models.ForeignKey('VGListUser',blank=False,null=False,related_name="friends_active")
	
	status = models.IntegerField(default=0, choices=STATUS_CHOICES.choices())
	
	time = models.DateTimeField(default=timezone.now,blank=False,null=False);
	
	class Meta:
		unique_together = [
			["vglistuser_active","vglistuser_passive"]
		]
		
		index_together = [
			["vglistuser_active","vglistuser_passive"]
		]
	
	def __unicode__(self):
		return "<%s,%s>: %s" % (self.vglistuser_active.__unicode__(), self.vglistuser_passive.__unicode__(), self.get_status_display())

@receiver(pre_save, sender=Friendship, dispatch_uid='swap_friends_pre_save')
def swap_friends_pre_save(sender, instance, using, **kwargs):
	if(instance.vglistuser_active and instance.vglistuser_passive):
		if(instance.vglistuser_active.pk > instance.vglistuser_passive.pk):
			temp = instance.vglistuser_active
			instance.vglistuser_active = instance.vglistuser_passive
			instance.vglistuser_passive = temp
			
class VGListStaff(models.Model):
	vglistuser = models.ForeignKey('VGListUser',blank=False,null=False)
	role = models.CharField(max_length=64,blank=False,null=False)
	
	def __unicode__(self):
		return "%s as %s" % (self.vglistuser.__unicode__(), self.role)

class NewsQuerySet(models.query.QuerySet):
	def filter_published(self):
		return self.exclude(time=None).exclude(time__gt=timezone.now())
		
	def exclude_published(self):
		return self.filter(time=None) | self.filter(time__gt=timezone.now())

class NewsManager(models.Manager):
	def get_queryset(self):
		return NewsQuerySet(self.model, using=self._db)

	def filter_published(self):
		return self.get_queryset().filter_published()

	def exclude_published(self):
		return self.get_queryset().exclude_published()

class News(models.Model):
	poster = models.ForeignKey('VGListUser',blank=False,null=False)
	title = models.CharField(default='',max_length=64,null=False)
	time = models.DateTimeField(blank=True,null=True);
	
	body = models.TextField(blank=True)
	
	comments = generic.GenericRelation('VGListComment')
	
	objects = NewsManager()
	
	def is_public(self):
		return self.time != None and self.time <= timezone.now()
	
	def __unicode__(self):
		return "#%s: %s" % (self.pk,self.title)

class VGListEditState(EditState):
	user = models.ForeignKey('VGListUser',blank=False,null=False)
	
	def __unicode__(self):
		return "Update on object %s from user %s" % (super(VGListEditState,self).__unicode__(), self.user.__unicode__())
